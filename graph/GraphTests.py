import numpy as np
import unittest
from graph.nodes.Node import Node
from graph.nodes.AddNode import AddNode
from graph.nodes.LinearNode import LinearNode
from graph.nodes.DebugNode import DebugNode
from graph.nodes.ReluNode import ReluNode
from graph.nodes.Im2ColNode import Im2ColNode
from graph.nodes.Col2ImNode import Col2ImNode
from graph.nodes.ConvNode import ConvNode
from graph.nodes.ConvNodeStride import ConvNodeStride
from graph.nodes.CrossEntropyLossNode import CrossEntropyLossNode
from graph.nodes.ReshapeNode import ReshapeNode
from graph.nodes.TransposeNode import TransposeNode
from graph.nodes.RelativeFreqNode import RelativeFreqNode
from graph.nodes.SoftmaxLossNode import SoftmaxLossNode
from graph.nodes.DropoutNode import DropoutNode
from graph.Graph import Graph

import warnings
import time

class GraphTest(unittest.TestCase):
    def test_node(self):

        #np.random.seed(0)
        shape=(2,2)
        g=Graph()
        g.layers.append([])
        g.layers.append([])
        n1=Node("n1",shape)
        n2=AddNode("n2",shape,-1)
        g.layers[0].append(n1)
        g.layers[1].append(n2)

        g.connect(n1,"y",n2,"x")
        input=np.random.rand(*shape)
        n1.x=input
        g.forward()
        output=n2.y

    def test_gradient(self):
        #np.random.seed(0)
        shape = (2, 2)
        g = Graph()
        g.layers.append([])
        g.layers.append([])
        n1 = Node("n1", shape)
        n2 = AddNode("n2", shape, -1)
        g.layers[0].append(n1)
        g.layers[1].append(n2)

        g.connect(n1, "y", n2, "x")

        g.register_input(shape,n1,"x")
        g.register_output(shape,n2,"y")

        for eps in [0.01,0.001,0.0001]:
            input1 = np.abs(np.random.rand(*shape))+0.1

            n1.x = input1
            g.forward()
            output1=np.empty((n2.y.shape))
            output1[:] = n2.y[:]

            eps=0.1
            input2=input1+eps

            n1.x=input2
            g.forward()
            output2=n2.y

            delta=output2-output1
            der=delta/eps

            grad_start=np.ones(shape)
            n2.d_y[:]=grad_start
            g.backward()
            der2=np.empty(shape)
            der2[:]=n1.d_x

            check=np.allclose(der2,der)
            if not check:
                print("just for debugging.")
            self.assertTrue(check)

    def test_addnode(self):
        #np.random.seed(0)
        shape = (2, 1)
        g = Graph()
        g.layers.append([])
        g.layers.append([])
        g.layers.append([])
        n1 = Node("n1", shape)
        n2=AddNode("n2",shape,-1)
        db=DebugNode("db",shape)
        g.layers[0].append(n1)
        g.layers[1].append(n2)
        g.layers[2].append(db)

        g.connect(n1,"y",n2,"x")
        g.connect(n2,"y",db,"x")

        g.register_input(shape, n1, "x")
        g.register_output((1,), db, "y")
        self.assertTrue(g.grad_check(-5))

    def test_linnode(self):
        np.random.seed(0)
        batch_size=5
        in_nodes=4
        out_nodes=3
        in_shape=(in_nodes,batch_size)
        out_shape=(out_nodes,batch_size)
        g=Graph()
        g.layers.append([])
        g.layers.append([])

        W=np.abs(np.random.rand(out_nodes,in_nodes))
        b=np.random.rand(out_nodes,1)
        weights=np.random.rand(out_nodes,1)
        ln=LinearNode("name",batch_size,in_nodes,out_nodes,W,b)
        db = DebugNode("db",out_shape,weights)
        g.nodes.append(ln)
        g.nodes.append(db)
        g.layers[0].append(ln)
        g.layers[1].append(db)
        g.connect(ln,"y",db,"x")
        g.register_input(in_shape,ln,"x")
        g.register_output(1,db,"y")

        self.assertTrue(g.grad_check(-7))

    def test_relunode(self):
        shape=(2,1)
        g=Graph()
        g.layers.append([])
        g.layers.append([])

        relu=ReluNode("relu",shape)
        db=DebugNode("db",shape,np.ones(shape))

        g.layers[0].append(relu)
        g.layers[1].append(db)

        g.nodes.append(relu)
        g.nodes.append(db)

        g.connect(relu,"y",db,"x")
        g.register_input(shape,relu,"x")
        g.register_output(shape,db,"y")

        self.assertTrue(g.grad_check(-7))

    def test_im2col(self):
        return

        batch_size=5
        n_colors=3
        width=10
        height=10

        inshape=(batch_size,n_colors,width,height)

        g = Graph()
        g.layers.append([])
        g.layers.append([])

        im2col = Im2ColNode("im2col",batch_size,n_colors,width,height)
        db = DebugNode("db", (9*n_colors,batch_size*width*height))

        g.layers[0].append(im2col)
        g.layers[1].append(db)

        g.nodes.append(im2col)
        g.nodes.append(db)

        g.connect(im2col, "y", db, "x")
        g.register_input(inshape, im2col, "x")
        g.register_output(1, db, "y")

        self.assertTrue(g.grad_check(-7))

    def test_col2im(self):
        return
        batch_size = 5
        n_colors = 3
        width = 10
        height = 10

        inshape = ( n_colors,batch_size* width* height)

        g = Graph()
        g.layers.append([])
        g.layers.append([])

        col2im = Col2ImNode("col2im", batch_size, n_colors, width, height)
        db = DebugNode("db", (batch_size,n_colors,width,height))

        g.layers[0].append(col2im)
        g.layers[1].append(db)

        g.nodes.append(col2im)
        g.nodes.append(db)

        g.connect(col2im, "y", db, "x")
        g.register_input(inshape, col2im, "x")
        g.register_output(1, db, "y")

        g.input[:]=np.random.randn(*inshape)
        g.forward()

        self.assertTrue(g.grad_check(-7))

    def test_conv_node(self):
        return
        batch_size = 5
        in_colors = 3
        out_colors=2
        width = 10
        height = 10

        W=np.random.randn(out_colors,9*in_colors)
        b=np.random.randn(out_colors,1)

        inshape = (batch_size,in_colors,width,height)

        g = Graph()
        g.layers.append([])
        g.layers.append([])

        conv_node = ConvNode("conv",W,b,batch_size,in_colors,out_colors,width,height)
        db = DebugNode("db", (batch_size, out_colors, width, height))

        g.layers[0].append(conv_node)
        g.layers[1].append(db)

        g.nodes.append( conv_node)
        g.nodes.append(db)

        g.connect( conv_node, "y", db, "x")
        g.register_input(inshape,  conv_node, "x")
        g.register_output(1, db, "y")

        g.input[:] = np.random.randn(*inshape)
        g.forward()

        self.assertTrue(g.grad_check(-7))

    def test_conv_node_stride(self):
        warnings.warn("disabled test_conv_node_stride")
        return
        batch_size = 2
        in_colors = 3
        out_colors = 2
        in_width = 10
        in_height = 10

        for i in range(1):
            for j in range(3):
                for k in range(4):
                    filter_size=i+1
                    stride=j
                    padding=k

                    if stride>0:
                        if (in_width - filter_size + 2 * padding) % stride!=0:
                            continue

                        out_width = (in_width - filter_size + 2 * padding) / stride + 1
                        out_height = (in_height - filter_size + 2 * padding) / stride + 1

                    else:
                        out_width = in_width - filter_size + 2 * padding + 1
                        out_height = in_height - filter_size + 2 * padding + 1

                    W = np.random.randn(out_colors, filter_size**2 * in_colors)
                    b = np.random.randn(out_colors, 1)

                    inshape = (batch_size, in_colors, in_width, in_height)

                    g = Graph()
                    g.layers.append([])
                    g.layers.append([])

                    conv_node = ConvNodeStride("conv",filter_size,padding,stride, W, b, batch_size, in_colors, out_colors, in_width, in_height)
                    db = DebugNode("db", (batch_size, out_colors, out_width, out_height))

                    g.layers[0].append(conv_node)
                    g.layers[1].append(db)

                    g.nodes.append(conv_node)
                    g.nodes.append(db)

                    g.connect(conv_node, "y", db, "x")
                    g.register_input(inshape, conv_node, "x")
                    g.register_output(1, db, "y")

                    g.input[:] = np.random.randn(*inshape)

                    print("---")
                    print(filter_size)
                    print(padding)
                    print(stride)
                    g.forward()

                    self.assertTrue(g.grad_check(-7))



    def test_reshapenode(self):
        np.random.seed(0)
        in_shape = (4,5)
        out_shape = (2, 10)
        g = Graph()
        g.layers.append([])
        g.layers.append([])

        weights = np.random.rand(2, 1)
        rn = ReshapeNode("rn",in_shape,out_shape)
        db = DebugNode("db", out_shape, weights)
        g.nodes.append(rn)
        g.nodes.append(db)
        g.layers[0].append(rn)
        g.layers[1].append(db)
        g.connect(rn, "y", db, "x")
        g.register_input(in_shape, rn, "x")
        g.register_output(1, db, "y")

        self.assertTrue(g.grad_check(-7))

    def test_relativeFreqNode(self):
        np.random.seed(0)
        nodes=6
        batch_size=10
        in_shape = (nodes, batch_size)
        g = Graph()
        g.layers.append([])
        g.layers.append([])

        weights = np.abs(np.random.rand(*in_shape))
        tn = RelativeFreqNode("rf",in_shape)
        db = DebugNode("db", in_shape, weights)
        g.nodes.append(tn)
        g.nodes.append(db)
        g.layers[0].append(tn)
        g.layers[1].append(db)
        g.connect(tn, "y", db, "x")
        g.register_input(in_shape, tn, "x")
        g.register_output(1, db, "y")

        logrange=-7
        # np.random.seed(0)
        for eps in np.logspace(-3, logrange, 10):
            sample = np.abs(np.random.rand(*g.input.shape))
            g.input[:] = sample
            g.forward()
            output1 = np.empty(g.output.shape)
            output1[:] = g.output[:]

            g.backward()
            d_input = np.empty(g.d_input.shape)
            d_input[:] = g.d_input[:]

            tolerance = 1e-4

            for mask in range(nodes):
                delta = np.zeros(g.input.shape)
                delta[mask,0] = 1 * eps
                g.input[:] = sample + delta
                g.forward()
                output2 = np.empty(g.output.shape)
                output2[:] = g.output

                der_numeric = (output2 - output1) / eps

                if not abs(der_numeric - d_input[mask,0]) < tolerance:
                    raise Exception("relativefreq failed")

        return True

    def test_transposenode(self):
        np.random.seed(0)
        in_shape = (4, 5, 6, 7, 1)
        permutation=(1,3,2,0,4)
        out_shape=(5,7,6,4,1)
        g = Graph()
        g.layers.append([])
        g.layers.append([])

        weights = np.random.rand(*out_shape)
        tn = TransposeNode("rn", in_shape, permutation)
        db = DebugNode("db", out_shape, weights)
        g.nodes.append(tn)
        g.nodes.append(db)
        g.layers[0].append(tn)
        g.layers[1].append(db)
        g.connect(tn, "y", db, "x")
        g.register_input(in_shape, tn, "x")
        g.register_output(1, db, "y")

        self.assertTrue(g.grad_check(-7))

    def test_softmax_loss(self):
        batch_size = 1
        n_classes=10
        g = Graph()
        g.layers.append([])

        # 7th node: CrossEntropyLoss
        sml = SoftmaxLossNode("softmaxloss", batch_size, n_classes)
        g.layers[0].append(sml)
        g.register_input((n_classes,batch_size),sml,"x")
        g.register_labels(sml, "labels")
        g.register_output((1, batch_size), sml, "y")

        g.labels[:] = np.ones((batch_size,))

        g.grad_check(-7)

    def test_dropout_node(self):
        np.random.seed(0)
        batch_size = 5
        in_nodes = 4
        in_shape = (in_nodes, batch_size)
        out_shape=(1,batch_size)
        g = Graph()
        g.layers.append([])
        g.layers.append([])
        weights = np.random.rand(*out_shape)
        dp=DropoutNode("dropout",in_shape,0.5)
        db = DebugNode("db", out_shape, weights)
        g.nodes.append(dp)
        g.nodes.append(db)
        g.layers[0].append(dp)
        g.layers[1].append(db)
        g.connect(dp, "y", db, "x")
        g.register_input(in_shape, dp, "x")
        g.register_output(1, db, "y")

        g.grad_check(-7)


