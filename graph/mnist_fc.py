import numpy as np
from graph.Graph import Graph
import numpy as np
import unittest
from graph.nodes.Node import Node
from graph.nodes.AddNode import AddNode
from graph.nodes.LinearNode import LinearNode
from graph.nodes.DebugNode import DebugNode
from graph.nodes.ReluNode import ReluNode
from graph.nodes.Im2ColNode import Im2ColNode
from graph.nodes.Col2ImNode import Col2ImNode
from graph.nodes.ConvNode import ConvNode
from graph.nodes.ConvNodeStride import ConvNodeStride
from graph.nodes.CrossEntropyLossNode import CrossEntropyLossNode
from graph.nodes.ReshapeNode import ReshapeNode
from graph.nodes.TransposeNode import TransposeNode
from graph.nodes.SoftmaxLossNode import SoftmaxLossNode
import pickle, gzip, numpy
f=gzip.open("../data/mnist.pkl.gz","rb")
train_set, valid_set, test_set=pickle.load(f, encoding="latin1")
f.close()
import time

#divide training set into minibatches
batches=[]
samples=50000
batch_size=100
assert samples%batch_size==0
n_batches=int(samples/batch_size)
for i in range(n_batches):
    batches.append((train_set[0][batch_size*i:batch_size*(i+1)],train_set[1][batch_size*i:batch_size*(i+1)]))

validation_batches=[]
validation_samples=10000
assert validation_samples%batch_size==0
n_validation_batches=int(validation_samples/batch_size)
for i in range(n_validation_batches):
    validation_batches.append((valid_set[0][batch_size*i:batch_size*(i+1)],valid_set[1][batch_size*i:batch_size*(i+1)]))





# I intend to do the following:
# 1: input layer (batch_size, 784)
# 2: reshape (batchsize,1,28,28)
# 3: ConvNode: filtersize=3, stride=0, padding=1
#              out_features=4
# The idea is to allow checking for borders - | \ /
# 4: FC Node
# 5: CrossEntropy Loss

g=Graph()

#add layers to the graph
#reshape
#conv
#reshape
#transpose
#fc
#relu
#crossentropy
for i in range(7):
    g.layers.append([])

#1st node: Reshape
shape_01=(batch_size,784)

#now transpose it
shape_12=(784, batch_size)
trans=TransposeNode("transpose",shape_01,(1,0))
g.layers[0].append(trans)
g.register_input(shape_01,trans,"x")

#now we can apply the FC node
#from the out_cols*in_height*in_width applied filters
#we need to determine the probability for each class

in_nodes_1=784
out_nodes_1=50
#TODO: move init of W and b somewhere centralized
W1=0.01 * np.random.randn(out_nodes_1, in_nodes_1)
b1=0* np.random.randn(out_nodes_1, 1)
shape_23=(out_nodes_1, batch_size)
fc1=LinearNode("fc1", batch_size, in_nodes_1, out_nodes_1, W1, b1)
g.layers[1].append(fc1)
g.connect(trans,"y",fc1,"x")

#6st node: ReluNode
rel1=ReluNode("relu",shape_23)
g.layers[2].append(rel1)
g.connect(fc1,"y",rel1,"x")

in_nodes_2=50
out_nodes_2=10
#TODO: move init of W and b somewhere centralized
W2=0.01*np.random.randn(out_nodes_2, in_nodes_2)
b2=0*np.random.randn(out_nodes_2, 1)
shape_45=(out_nodes_2, batch_size)
fc2=LinearNode("fc", batch_size, in_nodes_2, out_nodes_2, W2, b2)
g.layers[3].append(fc2)
g.connect(rel1,"y",fc2,"x")

#6st node: ReluNode
rel2=ReluNode("relu",shape_45)
g.layers[4].append(rel2)
g.connect(fc2,"y",rel2,"x")

#7th node: SoftmaxLosNode
sml=SoftmaxLossNode("softmaxloss",batch_size,out_nodes_2)
g.layers[5].append(sml)
g.connect(rel2,"y",sml,"x")
g.register_labels(sml,"labels")
g.register_output((1,batch_size),sml,"y")

params={}
d_params={}

#what params do we need to learn ?
# W,b from ConvNode
# W,b from FC

params["W_fc1"]=fc1.W
params["b_fc1"]=fc1.b
params["W_fc2"]=fc2.W
params["b_fc2"]=fc2.b

#preparation for rmsprop: list of momentum and derivative.
#for the first approach, I will only use learning rate.
d_params["d_W_fc1"]=[0,fc1.d_W]
d_params["d_b_fc1"]=[0,fc1.d_b]
d_params["d_W_fc2"]=[0,fc2.d_W]
d_params["d_b_fc2"]=[0,fc2.d_b]

trainer={}
trainer["W_fc1"]=[fc1.W,fc1.d_W, np.zeros(fc1.d_W.shape)]
trainer["b_fc1"]=[fc1.b,fc1.d_b, np.zeros(fc1.d_b.shape)]
trainer["W_fc2"]=[fc2.W,fc2.d_W,np.zeros(fc2.d_W.shape)]
trainer["b_fc2"]=[fc2.b,fc2.d_b,np.zeros(fc2.d_b.shape)]
epochs=90
decay=0.99
learning_rate=0.0005
eps=1e-7
start=time.time()
penalty=0.01

#now set up a plotting environment
import matplotlib.pyplot as plt

plt.ion()
plt.pause(0.01)
plt.figure("lrate:"+str(learning_rate)+", decay:"+str(decay)+", penalty:"+str(penalty))
plt.subplot(211)
ln1,=plt.plot([0],[0], "r--",lw=2)
plt.ylabel("loss")
plt.xlim([0,epochs])
plt.ylim([0,1])
plt.pause(0.1)
plt.subplot(212)
ln2,=plt.plot([0],[0], "b",lw=2)
plt.ylabel("validation accuracy")
plt.xlim([0,epochs])
plt.ylim([0,1])
plt.pause(0.1)
plt.show()
plt.pause(0.1)

plt_epochs=[]
plt_loss=[]
plt_accuracy=[]

for i in range(epochs):
    local_loss=np.zeros((1,batch_size))
    for batch in batches:
        g.input[:]=batch[0]
        g.labels[:]=batch[1].reshape(batch_size)
        g.forward()
        local_loss+=sml.y
        g.backward()
        for name, param_data in trainer.items():
            """
            update=learning_rate*(param_data[1]+penalty*param_data[0])
            if np.max(update / param_data[0]) > 1:
                pass
                #print("updating too fast")
            if np.max(param_data[0]) > 1e3:
                print("too much")
            param_data[0]-=update
            """
            d_param=param_data[1]+penalty*param_data[0]
            param_data[2][:]=decay*param_data[2]+(1-decay)*np.square(d_param)
            param_data[0]+= - learning_rate*d_param/(np.sqrt(param_data[2])+eps)


        #accuracy
    plt_loss.append(np.average(local_loss / n_batches))
    plt_epochs.append(i)

    hits=0
    misses=0
    for validation_batch in validation_batches:
        g.input[:]=validation_batch[0]
        g.labels[:]=validation_batch[1]
        g.forward()
        output=fc2.y
        for s in range(output.shape[1]):
            predicted=np.argmax(output[:,s])
            if predicted==validation_batch[1][s]:
                hits+=1
            else:
                misses+=1
    """
    for i in range(len(valid_set[0])):
        valid_input=valid_set[0][i]
        valid_label=valid_set[1][i]
        g.input[:]=np.zeros((batch_size,784))
        g.input[0,:]=valid_input
        g.forward()
        output=fc2.y[:,0]
        index=np.argmax(output)
        if index==valid_label:
            hits+=1
        else:
            misses+=1
    """

    print("hits: "+str(hits))
    print("misses: "+str(misses))
    plt_accuracy.append(hits/(hits+misses))
    ln2.set_xdata(plt_epochs)
    ln2.set_ydata(plt_accuracy)
    ln1.set_xdata(plt_epochs)
    ln1.set_ydata(plt_loss)

    plt.pause(0.1)

end=time.time()

np.save("W_fc1",fc1.W)
np.save("W_fc2",fc2.W)
np.save("b_fc1",fc1.b)
np.save("b_fc2",fc2.b)

print("took: "+str(end-start))

while True:
    plt.pause(1)





