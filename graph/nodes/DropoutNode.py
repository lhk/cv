import numpy as np
import numpy as np

class DropoutNode:
    def __init__(self, name, shape, probability=0.5):

        self.name=name
        self.shape=shape
        self.probability=probability

        # set up fields for the forward pass
        self.x=np.empty(shape)
        self.y=np.empty(shape)
        self.input={"x":shape}
        self.output={"y":shape}

        # set up members
        self.p=np.empty((shape[0],1))

        # set up fields for the backward pass
        self.d_x=np.empty(shape)
        self.d_y=np.empty(shape)
        self.d_output={"d_y":shape}
        self.d_input={"d_x":shape}

    def forward(self):
        self.p=np.random.rand(self.shape[0])
        weights=np.zeros(self.shape)
        weights[self.p>self.probability,:]=1
        self.y[:]=self.x*weights

    def backward(self):
        weights = np.zeros(self.shape)
        weights[self.p > self.probability,:] = 1
        self.d_x[:]=weights*self.d_y