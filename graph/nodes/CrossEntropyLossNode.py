import numpy as np

class CrossEntropyLossNode:
    def __init__(self, name, batch_size, n_classes):
        self.name=name
        self.shape=(n_classes,batch_size)
        # set up fields for the forward pass
        self.x=np.empty(self.shape)
        self.y=np.empty((batch_size,))
        self.input={"x":self.shape}
        self.output={"y":self.shape}

        # set up internal fields
        self.labels=np.empty((batch_size,),dtype=int)

        # set up fields for the backward pass
        self.d_x=np.empty(self.shape)
        self.d_y=np.empty((batch_size,))
        self.d_output={"d_y":self.shape}
        self.d_input={"d_x":self.shape}

    def forward(self):
        #store only the correct outputs in x
        #index list: zips the label with the batchnumber
        eps=1e-9
        temp=self.x[self.labels,np.arange(self.x.shape[1])]
        self.y[:]=-np.log(temp+eps)

    def backward(self):
        self.d_x[:]=0
        self.d_x[self.labels,np.arange(self.x.shape[1])]=-1/self.y*self.d_y