import numpy as np

class LinearNode:
    """

    """
    def __init__(self, name, batch_size, in_nodes, out_nodes, W, b):
        self.name=name
        self.batch_size=batch_size
        self.in_nodes=in_nodes
        self.out_nodes=out_nodes

        # set up fields for the forward pass
        self.x=np.empty((in_nodes,batch_size))
        self.y=np.empty((out_nodes,batch_size))
        self.input={"x":(in_nodes,batch_size)}
        self.output={"y":(out_nodes,batch_size)}

        # set up fields for the backward pass
        self.d_x=np.empty((in_nodes,batch_size))
        self.d_y=np.empty((out_nodes,batch_size))
        self.d_input={"d_y":(out_nodes,batch_size)}
        self.d_input={"d_x":(in_nodes,batch_size)}

        # set up params
        assert W.shape==(out_nodes,in_nodes)
        assert b.shape==(out_nodes,1)
        self.W=W
        self.b=b
        self.params={"W":W.shape,"b":b.shape}

        # set up params for the backward pass
        self.d_W=np.empty(W.shape)
        self.d_b=np.empty(b.shape)
        self.d_params={"d_W":W.shape,"d_b":b.shape}

    def forward(self):
        self.y[:]=(np.dot(self.W,self.x)+self.b)

    def backward(self):
        self.d_W[:]=np.dot(self.d_y,self.x.T)
        self.d_x[:]=np.dot(self.W.T,self.d_y)
        self.d_b[:]=np.sum(self.d_y, keepdims=True, axis=1)