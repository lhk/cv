import numpy as np

class AddNode:
    def __init__(self, name, shape, delta):
        self.name=name
        self.delta=delta

        # set up fields for the forward pass
        self.x=np.empty(shape)
        self.y=np.empty(shape)
        self.input={"x":shape}
        self.output={"y":shape}

        # set up fields for the backward pass
        self.d_x=np.empty(shape)
        self.d_y=np.empty(shape)
        self.d_input={"d_y":shape}
        self.d_input={"d_x":shape}

    def forward(self):
        self.y[:]=self.x + self.delta

    def backward(self):
        self.d_x[:]=self.d_y