import numpy as np
import numba


class Im2ColNode:
    def __init__(self, name, batch_size, n_colors, width, height):
        self.name = name
        self.batch_size = batch_size
        self.n_colors = n_colors
        self.width = width
        self.height = height

        # set up fields for the forward pass
        inshape = (batch_size, n_colors, width, height)
        outshape = (9 * n_colors, batch_size * width * height)
        self.x = np.empty(inshape)
        self.y = np.empty(outshape)
        self.input = {"x": inshape}
        self.output = {"y": outshape}

        # set up fields for the backward pass
        self.d_x = np.empty(inshape)
        self.d_y = np.empty(outshape)
        self.d_output = {"d_y": outshape}
        self.d_input = {"d_x": inshape}

    @numba.jit
    def forward2(self):
        x_padded = np.zeros((self.batch_size, self.n_colors, self.width + 2, self.height + 2))
        x_padded[:, :, 1:-1, 1:-1] = self.x

        self.y[:] = redistribute(x_padded, self.batch_size, self.n_colors, self.width,self.height)\
            .reshape((3 * 3 * self.n_colors, self.batch_size * self.width * self.height))

    @numba.jit(locals={"w":numba.i4,"h":numba.i4},cache=True)
    def forward(self):
        x_padded = np.zeros((self.batch_size, self.n_colors, self.width + 2, self.height + 2))
        x_padded[:, :, 1:-1, 1:-1] = self.x

        rec_fields = np.empty((3 * 3 * self.n_colors, self.batch_size, self.width, self.height))

        for w, h in np.ndindex((self.width, self.height)):
            rec_fields[:, :, w, h] = x_padded[:, :, w:w + 3, h:h + 3] \
                .reshape((self.batch_size, 3 * 3 * self.n_colors)) \
                .T

        self.y[:] = rec_fields.reshape((3 * 3 * self.n_colors, self.batch_size * self.width * self.height))

    def backward(self):
        d_rec_fields = self.d_y.reshape((3 * 3 * self.n_colors, self.batch_size, self.width, self.height))
        d_x_padded = np.zeros((self.batch_size, self.n_colors, self.width + 2, self.height + 2))

        for w, h in np.ndindex((self.width, self.height)):
            d_x_padded[:, :, w:w + 3, h:h + 3] += d_rec_fields[:, :, w, h].T.reshape(
                (self.batch_size, self.n_colors, 3, 3))

        self.d_x[:, :, :, :] = d_x_padded[:, :, 1:-1, 1:-1]


@numba.jit(numba.double[:, :, :, :](numba.double[:, :, :, :], numba.i4, numba.i4, numba.i4, numba.i4),cache=True)
def redistribute(x_padded, batch_size, n_colors, width, height):
    rec_fields = np.empty((3 * 3 * n_colors, batch_size, width, height))

    for w, h in np.ndindex((width, height)):
        rec_fields[:, :, w, h] = x_padded[:, :, w:w + 3, h:h + 3] \
            .reshape((batch_size, 3 * 3 * n_colors)) \
            .T
    return rec_fields
