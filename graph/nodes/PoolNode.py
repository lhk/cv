import numpy as np
import numba


class PoolNode():
    def __init__(self, name, batch_size, colors, width, height):
        self.batch_size=batch_size
        self.colors=colors
        self.width=width
        self.height = height
        self.name = name

        assert (width % 2 == 0)
        assert (height % 2 == 0)
        self.inshape=(batch_size,colors,width,height)
        self.outshape=(batch_size,colors,width/2,height/2)

        #set up fields for the forward pass
        self.x=np.empty(self.inshape)
        self.y=np.empty(self.outshape)
        self.input = {"x": self.inshape}
        self.output = {"y": self.outshape}
        self.predicate = np.zeros(self.inshape, dtype=np.bool)

        #set up fields for backward pass
        self.d_x=np.empty(self.inshape)
        self.d_y=np.empty(self.outshape)
        self.d_input={"x":self.inshape}
        self.d_output={"y":self.outshape}

    def forward(self):
        self.ll = self.x[:, :, ::2, ::2] # left lower
        self.rl = self.x[:, :, 1::2, ::2]#right lower
        self.lu = self.x[:, :, ::2, 1::2]#left upper
        self.ru = self.x[:, :, 1::2, 1::2]#right upper

        self.y[:]=np.maximum(self.ll, self.rl)
        self.y[:]=np.maximum(self.lu, self.y)
        self.y[:]=np.maximum(self.ru, self.y)

        pred_ll = self.ll == self.y
        pred_rl = self.rl == self.y
        pred_lu = self.lu == self.y
        pred_ru = self.ru == self.y

        self.predicate[:, :, ::2, ::2] = pred_ll
        self.predicate[:, :, 1::2, ::2] = pred_rl
        self.predicate[:, :, ::2, 1::2] = pred_lu
        self.predicate[:, :, 1::2, 1::2] = pred_ru

        #TODO: what if two are equal in size ? apply something like xor to the predicates

    def backward(self):
        self.d_x[:] = 0
        self.d_x[self.predicate] = self.d_y.reshape(-1)