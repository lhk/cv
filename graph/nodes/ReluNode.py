import numpy as np

class ReluNode:
    def __init__(self, name, shape):
        self.name=name

        # set up fields for the forward pass
        self.x=np.empty(shape)
        self.y=np.empty(shape)
        self.input={"x":shape}
        self.output={"y":shape}

        #set up parameters
        self.pred=np.zeros(self.y.shape,dtype=bool)

        # set up fields for the backward pass
        self.d_x=np.empty(shape)
        self.d_y=np.empty(shape)
        self.d_input={"d_y":shape}
        self.d_input={"d_x":shape}

    def forward(self):
        self.y[:]=self.x
        self.pred=self.y[:]<0
        self.y[self.pred]=0

    def backward(self):
        self.d_x[:]=self.d_y
        self.d_x[self.pred]=0