import numpy as np

class ReshapeNode:
    """

    """
    def __init__(self, name, in_shape, out_shape):
        self.name=name
        self.in_shape=in_shape
        self.out_shape=out_shape

        # set up fields for the forward pass
        self.x=np.empty(self.in_shape)
        self.y=np.empty(self.out_shape)
        self.input={"x":self.in_shape}
        self.output={"y":self.out_shape}

        # set up fields for the backward pass
        self.d_x=np.empty(self.in_shape)
        self.d_y=np.empty(self.out_shape)
        self.d_input={"d_y":self.out_shape}
        self.d_input={"d_x":self.in_shape}


    def forward(self):
        self.y[:]=self.x.reshape(self.out_shape)

    def backward(self):
        self.d_x[:]=self.d_y.reshape(self.in_shape)