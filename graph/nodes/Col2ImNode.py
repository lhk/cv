import numpy as np

class Col2ImNode:
    def __init__(self, name, batch_size, n_colors, width, height):
        self.name=name
        self.batch_size = batch_size
        self.n_colors = n_colors
        self.width = width
        self.height = height

        # set up fields for the forward pass
        self.inshape=(n_colors,batch_size*width*height)
        self.outshape=(batch_size,n_colors,width,height)
        self.x=np.empty(self.inshape)
        self.y=np.empty(self.outshape)
        self.input={"x":self.inshape}
        self.output={"y":self.outshape}

        # set up fields for the backward pass
        self.d_x=np.empty(self.inshape)
        self.d_y=np.empty(self.outshape)
        self.d_output={"d_y":self.outshape}
        self.d_input={"d_x":self.inshape}

    def forward(self):
        t=self.x.reshape((self.n_colors,self.batch_size,self.width,self.height))
        self.y[:]=t.transpose(1,0,2,3)


    def backward(self):
        dt=self.d_y.transpose(1,0,2,3)
        self.d_x[:]=dt.reshape((self.n_colors,self.batch_size*self.width*self.height))