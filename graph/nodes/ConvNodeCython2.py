import numpy as np
import graph.convolutions_condensed_cython as cv
"""
merging im2col, linear and col2im
"""
import numpy as np
import numba

"""
merging im2col, linear and col2im

the calculations are moved to an external module
"""

class ConvNodeStride:
    def __init__(self, name, filter_size, padding, stride, W, b, batch_size, in_colors, out_colors, in_width, in_height):
        self.name = name
        self.batch_size = batch_size
        self.in_colors=in_colors
        self.out_colors=out_colors
        self.in_width = in_width
        self.in_height = in_height

        assert((in_width-filter_size+2*padding)%stride==0)
        assert((in_height-filter_size+2*padding)%stride==0)

        self.padding=padding
        self.filter_size=filter_size
        self.stride=stride

        assert(self.padding>=0)
        assert(self.filter_size>0)
        assert(self.stride>0)

        self.out_width=(in_width-filter_size+2*padding)/stride+1
        self.out_height=(in_height-filter_size+2*padding)/stride+1

        # set up fields for the forward pass
        inshape = (batch_size, in_colors, in_width, in_height)
        outshape = (batch_size, out_colors, self.out_width, self.out_height)
        self.x = np.empty(inshape)
        self.y = np.empty(outshape)
        self.input = {"x": inshape}
        self.output = {"y": outshape}

        # internal fields
        self.in_cols=np.empty((self.filter_size ** 2 * self.in_colors, self.batch_size * self.out_width * self.out_height))

        # set up fields for the backward pass
        self.d_x = np.empty(inshape)
        self.d_y = np.empty(outshape)
        self.d_output = {"d_y": outshape}
        self.d_input = {"d_x": inshape}

        # set up params
        self.W = W
        self.b = b
        self.params = {"W": W.shape, "b": b.shape}

        # assert fitting shapes
        assert (W.shape==(out_colors,filter_size**2*in_colors))
        assert (b.shape==(out_colors,1))

        # set up params for the backward pass
        self.d_W = np.empty(W.shape)
        self.d_b = np.empty(b.shape)
        self.d_params = {"d_W": W.shape, "d_b": b.shape}


    #@numba.jit(locals={"w":numba.i4,"h":numba.i4},cache=True)
    def forward(self):
        #outsourced this call to a cython module.
        cv.forward(self.x, self.y,
                    self.filter_size, self.padding, self.stride,
                    self.W, self.b,
                    self.in_cols,
                    self.batch_size, self.in_colors, self.out_colors, self.in_width, self.in_height, self.out_width, self.out_height)
        return
        # im2col: x -> in_cols
        # padding
        x_padded = np.zeros((self.batch_size, self.in_colors, self.in_width + self.padding*2, self.in_height + self.padding*2))
        if self.padding>0:
            x_padded[:, :, self.padding:-self.padding, self.padding:-self.padding] = self.x
        else:
            x_padded[:,:,:,:]=self.x

        # allocating new field
        rec_fields = np.empty((self.filter_size**2* self.in_colors, self.batch_size, self.out_width, self.out_height))

        # copying receptive fields
        for w, h in np.ndindex((self.out_width, self.out_height)):
            rec_fields[:, :, w, h] = x_padded[:, :, w*self.stride:w*self.stride + self.filter_size, h*self.stride:h*self.stride + self.filter_size] \
                .reshape((self.batch_size, self.filter_size**2* self.in_colors)) \
                .T

        self.in_cols = rec_fields.reshape((self.filter_size**2 * self.in_colors, self.batch_size * self.out_width * self.out_height))

        # linear node: in_cols -> out_cols
        out_cols=np.dot(self.W,self.in_cols)+self.b

        # col2im: out_cols -> out_image -> y
        out_image = out_cols.reshape((self.out_colors, self.batch_size, self.out_width, self.out_height))
        self.y[:] = out_image.transpose(1, 0, 2, 3)

    def backward(self):
        # outsourced this call to a cython module.
        cv.backward(self.d_x, self.d_y,
                    self.filter_size, self.padding, self.stride,
                    self.W, self.b,
                    self.d_W, self.d_b,
                    self.in_cols,
                    self.batch_size, self.in_colors, self.out_colors, self.in_width, self.in_height, self.out_width, self.out_height)

        return
        # col2im: d_y -> d_out_cols
        d_out_image = self.d_y.transpose(1, 0, 2, 3)
        d_out_cols = d_out_image.reshape((self.out_colors, self.batch_size * self.out_width * self.out_height))

        # linear node: d_out_cols -> d_in_cols
        self.d_W[:] = np.dot(d_out_cols, self.in_cols.T)
        self.d_b[:] = np.sum(d_out_cols)
        d_in_cols = np.dot(self.W.T, d_out_cols)

        # im2col: d_in_cols -> d_x
        d_rec_fields = d_in_cols.reshape((self.filter_size**2 * self.in_colors, self.batch_size, self.out_width, self.out_height))
        d_x_padded = np.zeros((self.batch_size, self.in_colors, self.in_width + 2*self.padding, self.in_height + 2*self.padding))

        for w, h in np.ndindex((self.out_width, self.out_height)):
            d_x_padded[:, :, w*self.stride:w*self.stride + self.filter_size, h*self.stride:h*self.stride + self.filter_size] += d_rec_fields[:, :, w, h].T.reshape(
                (self.batch_size, self.in_colors, self.filter_size, self.filter_size))

        if self.padding>0:
            self.d_x[:, :, :, :] = d_x_padded[:, :, self.padding:-self.padding, self.padding:-self.padding]
        else:
            self.d_x[:,:,:,:]=d_x_padded[:,:,:,:]