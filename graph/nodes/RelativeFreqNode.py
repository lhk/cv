"""
this node is an experiment. I need to have relative frequencies in order to compute the cross entropy loss.
"""
import numpy as np

class RelativeFreqNode:
    def __init__(self, name, shape):
        self.name=name

        if(len(shape)!=2):
            raise Exception("RelativeFreqNode needs shape (nodes, batch_size)")

        # set up fields for the forward pass
        self.x=np.empty(shape)
        self.y=np.empty(shape)
        self.input={"x":shape}
        self.output={"y":shape}

        # set up fields for the backward pass
        self.d_x=np.empty(shape)
        self.d_y=np.empty(shape)
        self.d_input={"d_y":shape}
        self.d_input={"d_x":shape}

    def forward(self):
        self.old=np.sum(self.x)
        self.y[:]=self.x /np.sum(self.x)

    def backward(self):
        self.d_x[:]=self.d_y /self.old#/np.sum(self.x)