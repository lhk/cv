import numpy as np

class DebugNode:
    def __init__(self, name, shape, weights=None):
        self.name=name

        if np.any(weights==None):
            weights=np.random.rand(*shape)
        self.weights=weights
        # set up fields for the forward pass
        self.x=np.empty(shape)
        self.y=np.empty(1)
        self.input={"x":shape}
        self.output={"y":(1)}

        # set up fields for the backward pass
        self.d_x=np.empty(shape)
        self.d_y=np.empty(1)
        self.d_output={"d_y":1}
        self.d_input={"d_x":shape}

    def forward(self):
        self.y[:]=np.sum(self.x*self.weights)

    def backward(self):
        self.d_x[:]=self.weights*self.d_y