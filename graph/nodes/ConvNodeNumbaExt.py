import numpy as np
import graph.nodes.convolutions_numba as cv
import numba

"""
merging im2col, linear and col2im
actual calculations are outsourced to convolutions_numba
"""

class ConvNodeStride:
    def __init__(self, name, filter_size, padding, stride, W, b, batch_size, in_colors, out_colors, in_width, in_height):
        self.name = name
        self.batch_size = batch_size
        self.in_colors=in_colors
        self.out_colors=out_colors
        self.in_width = in_width
        self.in_height = in_height

        assert((in_width-filter_size+2*padding)%stride==0)
        assert((in_height-filter_size+2*padding)%stride==0)

        self.padding=padding
        self.filter_size=filter_size
        self.stride=stride

        assert(self.padding>=0)
        assert(self.filter_size>0)
        assert(self.stride>0)

        self.out_width=(in_width-filter_size+2*padding)/stride+1
        self.out_height=(in_height-filter_size+2*padding)/stride+1

        # set up fields for the forward pass
        inshape = (batch_size, in_colors, in_width, in_height)
        outshape = (batch_size, out_colors, self.out_width, self.out_height)
        self.x = np.empty(inshape)
        self.y = np.empty(outshape)
        self.input = {"x": inshape}
        self.output = {"y": outshape}

        # internal fields
        self.in_cols = np.empty((self.filter_size ** 2 * self.in_colors, self.batch_size * self.out_width * self.out_height))

        # set up fields for the backward pass
        self.d_x = np.empty(inshape)
        self.d_y = np.empty(outshape)
        self.d_output = {"d_y": outshape}
        self.d_input = {"d_x": inshape}

        # set up params
        self.W = W
        self.b = b
        self.params = {"W": W.shape, "b": b.shape}

        # assert fitting shapes
        assert (W.shape==(out_colors,filter_size**2*in_colors))
        assert (b.shape==(out_colors,1))

        # set up params for the backward pass
        self.d_W = np.empty(W.shape)
        self.d_b = np.empty(b.shape)
        self.d_params = {"d_W": W.shape, "d_b": b.shape}


    @numba.autojit(locals={"w":numba.i4,"h":numba.i4},cache=True)
    def forward(self):
        cv.forward(self.x, self.y,
                   self.filter_size, self.padding, self.stride,
                   self.W, self.b,
                   self.in_cols,
                   self.batch_size, self.in_colors, self.out_colors, self.in_width, self.in_height, self.out_width,
                   self.out_height)

    @numba.autojit(locals={"w":numba.i4,"h":numba.i4},cache=True)
    def backward(self):
        cv.backward(self.d_x, self.d_y,
                    self.filter_size, self.padding, self.stride,
                    self.W, self.b,
                    self.d_W, self.d_b,
                    self.in_cols,
                    self.batch_size, self.in_colors, self.out_colors, self.in_width, self.in_height, self.out_width,
                    self.out_height)
