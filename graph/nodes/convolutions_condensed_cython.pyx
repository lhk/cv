"""
this file works in tandem with ConvNodeCython2.
"""
cimport cython
cimport numpy as np

import numpy as np
import cython

DTYPE=np.float
ctypedef np.float_t DTYPE_t


@cython.boundscheck(False)
@cython.nonecheck(False)
def forward(np.ndarray[DTYPE_t, ndim=4] x, np.ndarray[DTYPE_t, ndim=4] y,
            int filter_size,int padding, int stride,
            np.ndarray[DTYPE_t, ndim=2] W, np.ndarray[DTYPE_t, ndim=2] b,
            np.ndarray[DTYPE_t, ndim=2] in_cols,
            int batch_size,int in_colors,int out_colors,int in_width,int in_height,int out_width,int out_height):
    """
    the forward method from a ConvNode.
    moved outside to compile it separately as a cython module
    :param x: input activations
    :param y: output activations
    :param filter_size: size of the receptive field
    :param padding: zero padding for the input
    :param stride: distance between receptive fields
    :param W: weight matrix for the linear node within
    :param b: bias for the linear node within
    :param batch_size: batch_size
    :param in_colors: number of input colors, first dimension of W
    :param out_colors: number of output colors, second dimension of W
    :param in_width: input activation image width
    :param in_height: input activation image height
    :param out_width: output activation image width
    :param out_height: output activation image height
    :return: nothing
    """

    # im2col: x -> in_cols
    # padding
    cdef np.ndarray[DTYPE_t, ndim=4] x_padded = np.zeros((batch_size, in_colors, in_width + padding*2, in_height + padding*2))
    if padding>0:
        x_padded[:, :, padding:in_width+padding, padding:in_height+padding] = x
    else:
        x_padded[:]=x

    # allocating new field
    cdef np.ndarray[DTYPE_t, ndim=4] rec_fields = np.empty((filter_size**2* in_colors, batch_size, out_width, out_height))

    # copying receptive fields
    cdef int w,h
    for w, h in np.ndindex((out_width, out_height)):
        rec_fields[:, :, w, h] = x_padded[:, :, w*stride:w*stride + filter_size, h*stride:h*stride + filter_size] \
            .reshape((batch_size, filter_size**2* in_colors)) \
            .T

    in_cols[:,:] = rec_fields.reshape((filter_size**2 * in_colors, batch_size * out_width * out_height))

    # linear node: in_cols -> out_cols
    #cdef np.ndarray[DTYPE_t, ndim=2] out_cols=np.dot(W,in_cols)+b

    # col2im: out_cols -> out_image -> y
    y[:,:,:,:]=(np.dot(W,in_cols)+b).reshape((out_colors, batch_size, out_width, out_height)).transpose(1,0,2,3)


@cython.boundscheck(False)
@cython.nonecheck(False)
def backward(np.ndarray[DTYPE_t, ndim=4] d_x,np.ndarray[DTYPE_t, ndim=4] d_y,
            int filter_size,int padding, int stride,
            np.ndarray[DTYPE_t, ndim=2] W, np.ndarray[DTYPE_t, ndim=2] b,
            np.ndarray[DTYPE_t, ndim=2] d_W, np.ndarray[DTYPE_t, ndim=2] d_b,
            np.ndarray[DTYPE_t, ndim=2] in_cols,
            int batch_size,int in_colors, int out_colors,int in_width,int in_height,int out_width, int out_height):

    # col2im: d_y -> d_out_cols
    cdef np.ndarray[DTYPE_t, ndim=2] d_out_cols =  d_y.transpose(1, 0, 2, 3).reshape((out_colors, batch_size * out_width * out_height))

    # linear node: d_out_cols -> d_in_cols
    d_W[:] = np.dot(d_out_cols, in_cols.T)
    d_b[:] = np.sum(d_out_cols)

    # im2col: d_in_cols -> d_x
    cdef np.ndarray[DTYPE_t, ndim=4] d_rec_fields = np.dot(W.T, d_out_cols).reshape((filter_size**2 * in_colors, batch_size, out_width, out_height))
    cdef np.ndarray[DTYPE_t, ndim=4] d_x_padded = np.zeros((batch_size, in_colors, in_width + 2*padding, in_height + 2*padding))

    cdef int w,h
    for w, h in np.ndindex((out_width, out_height)):
        d_x_padded[:, :, w*stride:w*stride + filter_size, h*stride:h*stride + filter_size] += d_rec_fields[:, :, w, h].T.reshape(
            (batch_size, in_colors, filter_size, filter_size))

    if padding>0:
        # slicing with non-negative indices
        # goes from [padding:-padding] = [padding: length-padding]
        # where length=in_width+2*padding
        d_x[:] = d_x_padded[:, :, padding:in_width+padding, padding:in_height+padding]
    else:
        d_x[:]=d_x_padded[:,:,:,:]