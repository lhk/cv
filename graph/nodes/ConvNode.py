import numpy as np
import numba

"""
merging im2col, linear and col2im
"""

class ConvNode:
    def __init__(self, name,W,b, batch_size, in_colors, out_colors, width, height):
        self.name = name
        self.batch_size = batch_size
        self.in_colors=in_colors
        self.out_colors=out_colors
        self.width = width
        self.height = height

        # set up fields for the forward pass
        inshape = (batch_size, in_colors, width, height)
        outshape = (batch_size, out_colors, width, height)
        self.x = np.empty(inshape)
        self.y = np.empty(outshape)
        self.input = {"x": inshape}
        self.output = {"y": outshape}

        # set up fields for the backward pass
        self.d_x = np.empty(inshape)
        self.d_y = np.empty(outshape)
        self.d_output = {"d_y": outshape}
        self.d_input = {"d_x": inshape}

        # set up params
        self.W = W
        self.b = b
        self.params = {"W": W.shape, "b": b.shape}

        # this ought to be based on the true output resolution
        # assert fitting shapes
        assert (W.shape==(out_colors,9*in_colors))
        assert (b.shape==(out_colors,1))

        # set up params for the backward pass
        self.d_W = np.empty(W.shape)
        self.d_b = np.empty(b.shape)
        self.d_params = {"d_W": W.shape, "d_b": b.shape}


    @numba.jit(locals={"w":numba.i4,"h":numba.i4},cache=True)
    def forward(self):

        # im2col: x -> in_cols
        # padding
        x_padded = np.zeros((self.batch_size, self.in_colors, self.width + 2, self.height + 2))
        x_padded[:, :, 1:-1, 1:-1] = self.x

        # allocating new field
        rec_fields = np.empty((3 * 3 * self.in_colors, self.batch_size, self.width, self.height))

        # copying receptive fields
        for w, h in np.ndindex((self.width, self.height)):
            rec_fields[:, :, w, h] = x_padded[:, :, w:w + 3, h:h + 3] \
                .reshape((self.batch_size, 3 * 3 * self.in_colors)) \
                .T

        self.in_cols = rec_fields.reshape((3 * 3 * self.in_colors, self.batch_size * self.width * self.height))

        # linear node: in_cols -> out_cols
        out_cols=np.dot(self.W,self.in_cols)+self.b

        # col2im: out_cols -> out_image -> y
        out_image = out_cols.reshape((self.out_colors, self.batch_size, self.width, self.height))
        self.y[:] = out_image.transpose(1, 0, 2, 3)


    def backward(self):

        # col2im: d_y -> d_out_cols
        d_out_image = self.d_y.transpose(1, 0, 2, 3)
        d_out_cols = d_out_image.reshape((self.out_colors, self.batch_size * self.width * self.height))

        # linear node: d_out_cols -> d_in_cols
        self.d_W[:] = np.dot(d_out_cols, self.in_cols.T)
        self.d_b[:] = np.sum(d_out_cols)
        d_in_cols = np.dot(self.W.T, d_out_cols)

        # im2col: d_in_cols -> d_x
        d_rec_fields = d_in_cols.reshape((3 * 3 * self.in_colors, self.batch_size, self.width, self.height))
        d_x_padded = np.zeros((self.batch_size, self.in_colors, self.width + 2, self.height + 2))

        for w, h in np.ndindex((self.width, self.height)):
            d_x_padded[:, :, w:w + 3, h:h + 3] += d_rec_fields[:, :, w, h].T.reshape(
                (self.batch_size, self.in_colors, 3, 3))

        self.d_x[:, :, :, :] = d_x_padded[:, :, 1:-1, 1:-1]
