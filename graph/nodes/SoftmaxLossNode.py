import numpy as np

class SoftmaxLossNode:
    def __init__(self, name, batch_size, n_classes):
        self.name=name
        self.batch_size=batch_size
        self.n_classes=n_classes
        self.shape=(n_classes,batch_size)
        # set up fields for the forward pass
        self.x=np.empty(self.shape)
        self.y=np.empty((batch_size,))
        self.input={"x":self.shape}
        self.output={"y":self.shape}

        # set up internal fields
        self.labels=np.empty((batch_size,),dtype=int)

        # set up fields for the backward pass
        self.d_x=np.empty(self.shape)
        self.d_y=np.empty((batch_size,))
        self.d_output={"d_y":self.shape}
        self.d_input={"d_x":self.shape}

    def forward(self):
        self.exp_scores=np.exp(self.x)
        self.probs=self.exp_scores/np.sum(self.exp_scores,axis=0,keepdims=True)
        self.y[:]=-np.log(self.probs[self.labels,np.arange(self.x.shape[1])])

    def backward(self):
        dscores=self.probs*self.d_y
        dscores[self.labels,np.arange(self.x.shape[1])]-=1
        dscores/=self.x.shape[1]
        self.d_x[:]=dscores




