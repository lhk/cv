
cimport cython

cimport numpy as np

import numpy as np
import cython

DTYPE=np.float
ctypedef np.float_t DTYPE_t

"""
merging im2col, linear and col2im
"""

cdef class ConvNodeStride:
    cdef public str name
    cdef int batch_size
    cdef int in_colors
    cdef int out_colors
    cdef int in_width
    cdef int in_height
    cdef int padding
    cdef int filter_size
    cdef int stride
    cdef int out_width
    cdef int out_height

    cdef public double[:,:,:,:] x
    cdef public object y
    cdef public double[:,:,:,:] d_x
    cdef public double[:,:,:,:] d_y
    cdef public double[:,:] in_cols
    cdef public double[:,:] W
    cdef public double[:,:] b
    cdef public double[:,:] d_W
    cdef public double[:,:] d_b

    cdef public dict input
    cdef public dict output
    cdef public dict d_input
    cdef public dict d_output
    cdef public dict params
    cdef public dict d_params

    def __init__(self, name, filter_size, padding, stride, W, b, batch_size, in_colors, out_colors, in_width, in_height):
        self.name = name
        self.batch_size = batch_size
        self.in_colors=in_colors
        self.out_colors=out_colors
        self.in_width = in_width
        self.in_height = in_height

        assert((in_width-filter_size+2*padding)%stride==0)
        assert((in_height-filter_size+2*padding)%stride==0)

        self.padding=padding
        self.filter_size=filter_size
        self.stride=stride

        assert(self.padding>=0)
        assert(self.filter_size>0)
        assert(self.stride>0)

        self.out_width=(in_width-filter_size+2*padding)/stride+1
        self.out_height=(in_height-filter_size+2*padding)/stride+1

        # set up fields for the forward pass
        inshape = (batch_size, in_colors, in_width, in_height)
        outshape = (batch_size, out_colors, self.out_width, self.out_height)
        self.x = np.empty(inshape)
        self.y = np.empty(outshape)
        self.input = {"x": inshape}
        self.output = {"y": outshape}

        # set up fields for the backward pass
        self.d_x = np.empty(inshape)
        self.d_y = np.empty(outshape)
        self.d_output = {"d_y": outshape}
        self.d_input = {"d_x": inshape}

        # set up params
        self.W = W
        self.b = b
        self.params = {"W": W.shape, "b": b.shape}

        # assert fitting shapes
        assert (W.shape==(out_colors,filter_size**2*in_colors))
        assert (b.shape==(out_colors,1))

        # set up params for the backward pass
        self.d_W = np.empty(W.shape)
        self.d_b = np.empty(b.shape)
        self.d_params = {"d_W": W.shape, "d_b": b.shape}

    @cython.boundscheck(False)
    @cython.nonecheck(False)
    def forward(self):

        # im2col: x -> in_cols
        # padding
        cdef np.ndarray[DTYPE_t, ndim=4] x_padded = np.zeros((self.batch_size, self.in_colors, self.in_width + self.padding*2, self.in_height + self.padding*2))
        if self.padding>0:
            x_padded[:, :, self.padding:self.in_width+self.padding, self.padding:self.in_height+self.padding] = self.x
        else:
            x_padded[:]=self.x

        # allocating new field
        cdef np.ndarray[DTYPE_t, ndim=4] rec_fields = np.empty((self.filter_size**2* self.in_colors, self.batch_size, self.out_width, self.out_height))

        # copying receptive fields
        cdef int w,h
        for w, h in np.ndindex((self.out_width, self.out_height)):
            rec_fields[:, :, w, h] = x_padded[:, :, w*self.stride:w*self.stride + self.filter_size, h*self.stride:h*self.stride + self.filter_size] \
                .reshape((self.batch_size, self.filter_size**2* self.in_colors)) \
                .T

        self.in_cols = rec_fields.reshape((self.filter_size**2 * self.in_colors, self.batch_size * self.out_width * self.out_height))

        # linear node: in_cols -> out_cols
        cdef np.ndarray[DTYPE_t, ndim=2] out_cols=np.dot(self.W,self.in_cols)+self.b

        # col2im: out_cols -> out_image -> y
        cdef np.ndarray[DTYPE_t, ndim = 4] out_image = out_cols.reshape((self.out_colors, self.batch_size, self.out_width, self.out_height))
        cdef double[:,:,:,:] temp
        temp=out_image.transpose(1,0,2,3)
        self.y[...] = temp

    @cython.boundscheck(False)
    @cython.nonecheck(False)
    def backward(self):

        # col2im: d_y -> d_out_cols
        cdef np.ndarray[DTYPE_t, ndim = 4] temp = self.d_y
        cdef np.ndarray[DTYPE_t, ndim=4] d_out_image = temp.transpose(1, 0, 2, 3)

        cdef np.ndarray[DTYPE_t, ndim=2] d_out_cols = d_out_image.reshape((self.out_colors, self.batch_size * self.out_width * self.out_height))

        # linear node: d_out_cols -> d_in_cols
        self.d_W[:] = np.dot(d_out_cols, self.in_cols.T)
        self.d_b[:] = np.sum(d_out_cols)
        cdef np.ndarray[DTYPE_t, ndim=2] d_in_cols = np.dot(self.W.T, d_out_cols)

        # im2col: d_in_cols -> d_x
        cdef np.ndarray[DTYPE_t, ndim=4] d_rec_fields = d_in_cols.reshape((self.filter_size**2 * self.in_colors, self.batch_size, self.out_width, self.out_height))
        cdef np.ndarray[DTYPE_t, ndim=4] d_x_padded = np.zeros((self.batch_size, self.in_colors, self.in_width + 2*self.padding, self.in_height + 2*self.padding))

        cdef int w,h
        for w, h in np.ndindex((self.out_width, self.out_height)):
            d_x_padded[:, :, w*self.stride:w*self.stride + self.filter_size, h*self.stride:h*self.stride + self.filter_size] += d_rec_fields[:, :, w, h].T.reshape(
                (self.batch_size, self.in_colors, self.filter_size, self.filter_size))

        if self.padding>0:
            # slicing with non-negative indices
            # goes from [padding:-padding] = [padding: length-padding]
            # where length=in_width+2*padding
            self.d_x[:] = d_x_padded[:, :, self.padding:self.in_width+self.padding, self.padding:self.in_height+self.padding]
        else:
            self.d_x[:]=d_x_padded[:,:,:,:]