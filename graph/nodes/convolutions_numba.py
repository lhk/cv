
import numpy as np
import numba

@numba.autojit(cache=True,locals=dict(w=int,h=int,x=numba.double[:,:,:,:]))
def forward(x, y,
            filter_size,padding, stride,
            W, b,
            in_cols,
            batch_size,in_colors,out_colors,in_width,in_height,out_width,out_height):
    """
    the forward method from a ConvNode.
    moved outside to compile it separately as a cython module
    :param x: input activations
    :param y: output activations
    :param filter_size: size of the receptive field
    :param padding: zero padding for the input
    :param stride: distance between receptive fields
    :param W: weight matrix for the linear node within
    :param b: bias for the linear node within
    :param batch_size: batch_size
    :param in_colors: number of input colors, first dimension of W
    :param out_colors: number of output colors, second dimension of W
    :param in_width: input activation image width
    :param in_height: input activation image height
    :param out_width: output activation image width
    :param out_height: output activation image height
    :return: nothing
    """

    # im2col: x -> in_cols
    # padding
    x_padded = np.zeros((batch_size, in_colors, in_width + padding*2, in_height + padding*2))
    if padding>0:
        x_padded[:, :, padding:in_width+padding, padding:in_height+padding] = x
    else:
        x_padded[:]=x

    # allocating new field
    rec_fields = np.empty((filter_size**2* in_colors, batch_size, out_width, out_height))

    # copying receptive fields

    im2col(np.ascontiguousarray(x_padded),np.ascontiguousarray(rec_fields),out_width,out_height,filter_size, stride,batch_size,in_colors)
    """
    for w, h in np.ndindex((out_width, out_height)):
        rec_fields[:, :, w, h] = x_padded[:, :, w*stride:w*stride + filter_size, h*stride:h*stride + filter_size] \
            .reshape((batch_size, filter_size**2* in_colors)) \
            .T
    """

    in_cols[:,:] = rec_fields.reshape((filter_size**2 * in_colors, batch_size * out_width * out_height))

    # linear node: in_cols -> out_cols
    out_cols=np.dot(W,in_cols)+b

    # col2im: out_cols -> out_image -> y
    y[:,:,:,:]=out_cols.reshape((out_colors, batch_size, out_width, out_height)).transpose(1,0,2,3)

@numba.jit(nopython=True)
def im2col(x_padded,rec_fields,out_width,out_height,filter_size,stride,batch_size,in_colors):
    for w in range(out_width):
        for h in range(out_height):
            rec_fields[:, :, w, h] = x_padded[:, :, w * stride:w * stride + filter_size,
                                     h * stride:h * stride + filter_size] \
                .reshape((batch_size, filter_size ** 2 * in_colors)) \
                .T

def backward(d_x,d_y,
            filter_size,padding,stride,
            W, b,
            d_W,d_b,
            in_cols,
            batch_size,in_colors,out_colors,in_width,in_height,out_width,out_height):

    # col2im: d_y -> d_out_cols
    d_out_image = d_y.transpose(1, 0, 2, 3)
    d_out_cols = d_out_image.reshape((out_colors, batch_size * out_width * out_height))

    # linear node: d_out_cols -> d_in_cols
    d_W[:] = np.dot(d_out_cols, in_cols.T)
    d_b[:] = np.sum(d_out_cols)
    d_in_cols = np.dot(W.T, d_out_cols)

    # im2col: d_in_cols -> d_x
    d_rec_fields = d_in_cols.reshape((filter_size**2 * in_colors, batch_size, out_width, out_height))
    d_x_padded = np.zeros((batch_size, in_colors, in_width + 2*padding, in_height + 2*padding))

    for w, h in np.ndindex((out_width, out_height)):
        d_x_padded[:, :, w*stride:w*stride + filter_size, h*stride:h*stride + filter_size] += d_rec_fields[:, :, w, h].T.reshape(
            (batch_size, in_colors, filter_size, filter_size))

    if padding>0:
        # slicing with non-negative indices
        # goes from [padding:-padding] = [padding: length-padding]
        # where length=in_width+2*padding
        d_x[:] = d_x_padded[:, :, padding:in_width+padding, padding:in_height+padding]
    else:
        d_x[:]=d_x_padded[:,:,:,:]