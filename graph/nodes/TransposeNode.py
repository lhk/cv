import numpy as np

class TransposeNode:
    """

    """
    def __init__(self, name, in_shape,permutation):
        self.name=name
        self.permutation=permutation
        if len(self.permutation)==2:
            self.inv_perm=self.permutation
        else:
            self.inv_perm=tuple([permutation[x] for x in permutation])
        self.in_shape=in_shape

        # argh, had to transpose manually
        self.out_shape=tuple([in_shape[x] for x in permutation])

        # set up fields for the forward pass
        self.x=np.empty(self.in_shape)
        self.y=np.empty(self.out_shape)
        self.input={"x":self.in_shape}
        self.output={"y":self.out_shape}

        # set up fields for the backward pass
        self.d_x=np.empty(self.in_shape)
        self.d_y=np.empty(self.out_shape)
        self.d_input={"d_y":self.out_shape}
        self.d_input={"d_x":self.in_shape}


    def forward(self):
        self.y[:]=self.x.transpose(self.permutation)

    def backward(self):
        self.d_x[:]=self.d_y.transpose(self.inv_perm)