import numpy as np
from graph.Graph import Graph
import numpy as np
import unittest
from graph.nodes.Node import Node
from graph.nodes.AddNode import AddNode
from graph.nodes.LinearNode import LinearNode
from graph.nodes.DebugNode import DebugNode
from graph.nodes.ReluNode import ReluNode
from graph.nodes.Im2ColNode import Im2ColNode
from graph.nodes.Col2ImNode import Col2ImNode
from graph.nodes.ConvNode import ConvNode
#from graph.nodes.ConvNodeStride import ConvNodeStride
from graph.nodes.ConvNodeNumba import ConvNodeStride
from graph.nodes.CrossEntropyLossNode import CrossEntropyLossNode
from graph.nodes.ReshapeNode import ReshapeNode
from graph.nodes.TransposeNode import TransposeNode
from graph.nodes.SoftmaxLossNode import SoftmaxLossNode
import pickle, gzip, numpy
f=gzip.open("../data/mnist.pkl.gz","rb")
train_set, valid_set, test_set=pickle.load(f, encoding="latin1")
f.close()
import time

#divide training set into minibatches
batches=[]
samples=50000
batch_size=500
assert samples%batch_size==0
n_batches=int(samples/batch_size)
for i in range(n_batches):
    batches.append((train_set[0][batch_size*i:batch_size*(i+1)],train_set[1][batch_size*i:batch_size*(i+1)]))

validation_batches=[]
validation_samples=10000
assert validation_samples%batch_size==0
n_validation_batches=int(validation_samples/batch_size)
for i in range(n_validation_batches):
    validation_batches.append((valid_set[0][batch_size*i:batch_size*(i+1)],valid_set[1][batch_size*i:batch_size*(i+1)]))

#throw away some data
#batches=batches[:-len(batches)//2]
validation_batches=validation_batches[:-int(len(validation_batches)*0.8)]

# I intend to do the following:
# 1: input layer (batch_size, 784)
# 2: reshape (batchsize,1,28,28)
# 3: ConvNode: filtersize=3, stride=0, padding=1
#              out_features=4
# The idea is to allow checking for borders - | \ /
# 4: FC Node
# 5: Softmax Loss

g=Graph()

#add layers to the graph
#reshape
#conv
#reshape
#transpose
#fc
#relu
#crossentropy
for i in range(7):
    g.layers.append([])

#1st node: Reshape
shape_01=(batch_size,784)
shape_12=(batch_size,1,28,28)
rn_input=ReshapeNode("reshape input", shape_01, shape_12)
g.layers[0].append(rn_input)
g.register_input(shape_01, rn_input, "x")

#2nd node: Convolution
filter_size=3
padding=1
stride=0
in_cols=1
out_cols=4
in_width=28
in_height=28

W=0.01*np.random.randn(out_cols,in_cols*filter_size**2)
b=0*np.random.randn(out_cols,1)
#TODO: maybe move init of W and b inside the node ?
#or overwrite them in the trainer
cnv=ConvNodeStride("convnode",
                   filter_size,padding,stride,
                   W,b,
                   batch_size,in_cols,out_cols,
                   in_width,in_height)
g.layers[1].append(cnv)
g.connect(rn_input, "y", cnv, "x")

#3rd node: FC node (actually three nodes)
#There is a conflict in the layout of the nodes
#The LinearNode(fully connected) requires this shape:
#(nodes, batch_size)
#I'll add a reshape and a transpose node, too


#reshape the output from the ConvNode
shape_23=(batch_size,out_cols,in_width,in_height)
shape_34=(batch_size,out_cols*in_width*in_height)
rs_conv=ReshapeNode("rs conv",shape_23,shape_34)
g.layers[2].append(rs_conv)
g.connect(cnv,"y",rs_conv,"x")

#now transpose it
shape_45=(out_cols*in_height*in_width,batch_size)
trans=TransposeNode("transpose",shape_34,(1,0))
g.layers[3].append(trans)
g.connect(rs_conv,"y",trans,"x")

#now we can apply the FC node
#from the out_cols*in_height*in_width applied filters
#we need to determine the probability for each class
n_classes=10
in_nodes=out_cols*in_height*in_width
#TODO: see above, move init of W and b somewhere centralized
W=0.01*np.random.randn(n_classes,in_nodes)
b=0*np.random.randn(n_classes,1)
shape_56=(n_classes,batch_size)
fc=LinearNode("fc",batch_size,in_nodes,n_classes,W,b)
g.layers[4].append(fc)
g.connect(trans,"y",fc,"x")

#6st node: ReluNode
rel=ReluNode("relu",shape_56)
g.layers[5].append(rel)
g.connect(fc,"y",rel,"x")

#7th node: CrossEntropyLoss
sml=SoftmaxLossNode("softmaxloss",batch_size,n_classes)
g.layers[6].append(sml)
g.connect(rel,"y",sml,"x")
g.register_labels(sml,"labels")
g.register_output((1,batch_size),sml,"y")

params={}
d_params={}

#what params do we need to learn ?
# W,b from ConvNode
# W,b from FC

params["W_conv"]=cnv.W
params["b_conv"]=cnv.b
params["W_fc"]=fc.W
params["b_fc"]=fc.b

#preparation for rmsprop: list of momentum and derivative.
#for the first approach, I will only use learning rate.
d_params["d_W_conv"]=[0,cnv.d_W]
d_params["d_b_conv"]=[0,cnv.d_b]
d_params["d_W_fc"]=[0,fc.d_W]
d_params["d_b_fc"]=[0,fc.d_b]

trainer={}
trainer["W_conv"]=[cnv.W,cnv.d_W, np.zeros(cnv.d_W.shape)]
trainer["b_conv"]=[cnv.b,cnv.d_b, np.zeros(cnv.d_b.shape)]
trainer["W_fc"]=[fc.W,fc.d_W,np.zeros(fc.d_W.shape)]
trainer["b_fc"]=[fc.b,fc.d_b,np.zeros(fc.d_b.shape)]
decay=0.9
learning_rate=0.0015
eps=1e-7
start=time.time()
penalty=0.001

for i in range(1):
    for i in range(len(batches)):
        batch=batches[i]
        print("processing batch: "+str(i))
        g.input[:]=batch[0]
        g.labels[:]=batch[1].reshape(batch_size)
        g.forward()
        g.backward()
        for name, param_data in trainer.items():
            """
            update=learning_rate*(param_data[1]+penalty*param_data[0])
            if np.max(update / param_data[0]) > 1:
                pass
                #print("updating too fast")
            if np.max(param_data[0]) > 1e3:
                print("too much")
            param_data[0]-=update
            """
            d_param=param_data[1]+penalty*param_data[0]
            param_data[2][:]=decay*param_data[2]+(1-decay)*np.square(d_param)
            param_data[0]+= - learning_rate*d_param/(np.sqrt(param_data[2])+eps)


        #accuracy
    hits=0
    misses=0
    for validation_batch in validation_batches:
        g.input[:]=validation_batch[0]
        g.labels[:]=validation_batch[1]
        g.forward()
        output=rel.y
        for s in range(output.shape[1]):
            predicted=np.argmax(output[:,s])
            if predicted==validation_batch[1][s]:
                hits+=1
            else:
                misses+=1
    """
    for i in range(len(valid_set[0])):
        valid_input=valid_set[0][i]
        valid_label=valid_set[1][i]
        g.input[:]=np.zeros((batch_size,784))
        g.input[0,:]=valid_input
        g.forward()
        output=fc2.y[:,0]
        index=np.argmax(output)
        if index==valid_label:
            hits+=1
        else:
            misses+=1
    """

    print("hits: "+str(hits))
    print("misses: "+str(misses))

end=time.time()
print("took: "+str(end-start))

np.save("W_conv",cnv.W)
np.save("W_fc",fc.W)
np.save("b_conv",cnv.b)
np.save("b_fc",fc.b)






