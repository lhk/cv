import numpy as np
from graph.Graph import Graph
import numpy as np
import unittest
from graph.nodes.Node import Node
from graph.nodes.AddNode import AddNode
from graph.nodes.LinearNode import LinearNode
from graph.nodes.DebugNode import DebugNode
from graph.nodes.ReluNode import ReluNode
from graph.nodes.Im2ColNode import Im2ColNode
from graph.nodes.Col2ImNode import Col2ImNode
from graph.nodes.ConvNode import ConvNode
from graph.nodes.ConvNodeStride import ConvNodeStride
from graph.nodes.CrossEntropyLossNode import CrossEntropyLossNode
from graph.nodes.ReshapeNode import ReshapeNode
from graph.nodes.TransposeNode import TransposeNode
from graph.nodes.SoftmaxLossNode import SoftmaxLossNode
import pickle, gzip, numpy
import graph.GraphBuilder
f=gzip.open("../data/mnist.pkl.gz","rb")
train_set, valid_set, test_set=pickle.load(f, encoding="latin1")
f.close()
import time

#divide training set into minibatches
batches=[]
samples=50000
batch_size=100
assert samples%batch_size==0
n_batches=int(samples/batch_size)
for i in range(n_batches):
    batches.append((train_set[0][batch_size*i:batch_size*(i+1)],train_set[1][batch_size*i:batch_size*(i+1)]))

validation_batches=[]
validation_samples=10000
assert validation_samples%batch_size==0
n_validation_batches=int(validation_samples/batch_size)
for i in range(n_validation_batches):
    validation_batches.append((valid_set[0][batch_size*i:batch_size*(i+1)],valid_set[1][batch_size*i:batch_size*(i+1)]))



#trying the new graphbuilder functionality
out_nodes=[70,50,35,10]
g,trainer=graph.GraphBuilder.build_fc_graph(0.2,0,batch_size,784,out_nodes)

epochs=100
decay=0.85
learning_rate=0.00001
eps=1e-9
start=time.time()
penalty=0.0001

#now set up a plotting environment
import matplotlib.pyplot as plt

plt.ion()
plt.pause(0.01)
plt.figure("lrate:"+str(learning_rate)+", decay:"+str(decay)+", penalty:"+str(penalty)+" - "+str(out_nodes))
plt.subplot(211)
ln1,=plt.plot([0],[0], "r--",lw=2)
plt.ylabel("loss")
plt.xlim([0,epochs])
plt.ylim([0,1])
plt.pause(0.1)
plt.subplot(212)
ln2,=plt.plot([0],[0], "b",lw=2)
plt.ylabel("validation accuracy")
plt.xlim([0,epochs])
plt.ylim([0,1])
plt.pause(0.1)
plt.show()
plt.pause(0.1)

plt_epochs=[]
plt_loss=[]
plt_accuracy=[]

for i in range(epochs):
    local_loss=np.zeros((1,batch_size))
    for batch in batches:
        g.input[:]=batch[0].T
        g.labels[:]=batch[1].reshape(batch_size)
        g.forward()
        local_loss+=g.output
        g.backward()
        for name, param_data in trainer.items():
            """
            update=learning_rate*(param_data[1]+penalty*param_data[0])
            if np.max(update / param_data[0]) > 1:
                pass
                #print("updating too fast")
            if np.max(param_data[0]) > 1e3:
                print("too much")
            param_data[0]-=update
            """
            d_param=param_data[1]+penalty*param_data[0]
            param_data[2][:]=decay*param_data[2]+(1-decay)*np.square(d_param)
            param_data[0]+= - learning_rate*d_param/(np.sqrt(param_data[2])+eps)


        #accuracy
    plt_loss.append(np.average(local_loss / n_batches))
    plt_epochs.append(i)

    hits=0
    misses=0
    for validation_batch in validation_batches:
        g.input[:]=validation_batch[0].T
        g.labels[:]=validation_batch[1]
        g.forward()
        #TODO: dirty monkey patching. think of a better way to get slices of activations from the graph
        output=g.fc.y
        for s in range(output.shape[1]):
            predicted=np.argmax(output[:,s])
            if predicted==validation_batch[1][s]:
                hits+=1
            else:
                misses+=1

    print("hits: "+str(hits))
    print("misses: "+str(misses))
    plt_accuracy.append(hits/(hits+misses))
    ln2.set_xdata(plt_epochs)
    ln2.set_ydata(plt_accuracy)
    ln1.set_xdata(plt_epochs)
    ln1.set_ydata(plt_loss)

    plt.pause(0.1)

end=time.time()

print("took: "+str(end-start))

import pickle
f=open("training_results"+str(epochs)+"lrate"+str(learning_rate)+"decay"+str(decay)+"penalty"+str(penalty)+""+str(out_nodes)+".pickle", 'wb')
# Pickle the 'data' dictionary using the highest protocol available.
pickle.dump(trainer, f, pickle.HIGHEST_PROTOCOL)
f.close()

print("training_results"+str(epochs)+"lrate"+str(learning_rate)+"decay"+str(decay)+"penalty"+str(penalty)+""+str(out_nodes)+".pickle")

while True:
    plt.pause(1)





