from distutils.core import setup
from Cython.Build import cythonize
import numpy as np

setup(
    ext_modules=cythonize(["nodes/convolutions_condensed_cython.pyx","nodes/ConvNodeCython.pyx","GraphCython.pyx"]),
    include_dirs=[np.get_include()]
)