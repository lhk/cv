import numpy as np
import unittest
from graph.nodes.Node import Node
from graph.nodes.AddNode import AddNode
from graph.nodes.LinearNode import LinearNode
from graph.nodes.DebugNode import DebugNode
from graph.nodes.ReluNode import ReluNode
from graph.nodes.Im2ColNode import Im2ColNode
from graph.nodes.Col2ImNode import Col2ImNode
from graph.nodes.ConvNode import ConvNode
from graph.nodes.ConvNodeStride import ConvNodeStride
from graph.nodes.CrossEntropyLossNode import CrossEntropyLossNode
from graph.nodes.ReshapeNode import ReshapeNode
from graph.nodes.TransposeNode import TransposeNode
from graph.nodes.RelativeFreqNode import RelativeFreqNode
from graph.nodes.SoftmaxLossNode import SoftmaxLossNode
from graph.Graph import Graph
import unittest
class ComplexTests(unittest.TestCase):
    def test_conv_for_mnist(self):
        batch_size=1
        g = Graph()

        # add layers to the graph
        # reshape
        # conv
        # reshape
        # transpose
        # fc
        # relu
        # crossentropy
        for i in range(7):
            g.layers.append([])

        # 1st node: Reshape
        shape_01 = (batch_size, 784)
        shape_12 = (batch_size, 1, 28, 28)
        rn_input = ReshapeNode("reshape input", shape_01, shape_12)
        g.layers[0].append(rn_input)
        g.register_input(shape_01, rn_input, "x")

        # 2nd node: Convolution
        filter_size = 3
        padding = 1
        stride = 0
        in_cols = 1
        out_cols = 4
        in_width = 28
        in_height = 28

        W = np.random.randn(out_cols, in_cols * filter_size ** 2)
        b = np.random.randn(out_cols, 1)
        # TODO: maybe move init of W and b inside the node ?
        # or overwrite them in the trainer
        cnv = ConvNodeStride("convnode",
                             filter_size, padding, stride,
                             W, b,
                             batch_size, in_cols, out_cols,
                             in_width, in_height)
        g.layers[1].append(cnv)
        g.connect(rn_input, "y", cnv, "x")

        # 3rd node: FC node (actually three nodes)
        # There is a conflict in the layout of the nodes
        # The LinearNode(fully connected) requires this shape:
        # (nodes, batch_size)
        # I'll add a reshape and a transpose node, too


        # reshape the output from the ConvNode
        shape_23 = (batch_size, out_cols, in_width, in_height)
        shape_34 = (batch_size, out_cols * in_width * in_height)
        rs_conv = ReshapeNode("rs conv", shape_23, shape_34)
        g.layers[2].append(rs_conv)
        g.connect(cnv, "y", rs_conv, "x")

        # now transpose it
        shape_45 = (out_cols * in_height * in_width, batch_size)
        trans = TransposeNode("transpose", shape_34, (1, 0))
        g.layers[3].append(trans)
        g.connect(rs_conv, "y", trans, "x")

        # now we can apply the FC node
        # from the out_cols*in_height*in_width applied filters
        # we need to determine the probability for each class
        n_classes = 10
        in_nodes = out_cols * in_height * in_width
        # TODO: see above, move init of W and b somewhere centralized
        W = np.random.randn(n_classes, in_nodes)
        b = np.random.randn(n_classes, 1)
        shape_56 = (n_classes, batch_size)
        fc = LinearNode("fc", batch_size, in_nodes, n_classes, W, b)
        g.layers[4].append(fc)
        g.connect(trans, "y", fc, "x")

        # 6st node: ReluNode
        rel = ReluNode("relu", shape_56)
        g.layers[5].append(rel)
        g.connect(fc, "y", rel, "x")

        # 7th node: CrossEntropyLoss
        sml = SoftmaxLossNode("softmaxloss", batch_size, n_classes)
        g.layers[6].append(sml)
        g.connect(rel, "y", sml, "x")
        g.register_labels(sml, "labels")
        g.register_output((1, batch_size), sml, "y")

        g.labels[:]=np.ones((batch_size,))

        g.grad_check(-7)