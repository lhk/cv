import numpy as np

m=4
n=4
mat=np.random.rand(n,m)
#mat=np.ones((n,m))
for i in range(m):
    mat[i,i]=1

def norm(matrix):
    return np.sqrt(np.sum(np.square(mat)))

orig=norm(mat)**2

eps=1e-3

for id in np.ndindex((n,m)):
    mat[id]+=eps
    delta=norm(mat)**2-orig
    print(delta/eps/mat[id])
    mat[id]-=eps
