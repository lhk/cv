import numpy as np
import unittest
from graph.nodes.Node import Node
from graph.nodes.AddNode import AddNode
from graph.nodes.LinearNode import LinearNode
from graph.nodes.DebugNode import DebugNode
from graph.nodes.ReluNode import ReluNode
from graph.nodes.Im2ColNode import Im2ColNode
from graph.nodes.Col2ImNode import Col2ImNode
from graph.nodes.ConvNode import ConvNode
from graph.nodes.ConvNodeStride import ConvNodeStride
import time

cimport cython
import cython
"""
interesting ideas: create a node specifically for input purposes
"""
cdef class Graph:

    cdef public list layers
    cdef public dict results
    cdef public list nodes
    cdef public dict state
    cdef public object input
    cdef public object d_input
    cdef public object output
    cdef public object d_output
    def __init__(self):
        # a list of lists of nodes
        self.layers=[]

        # a dictionary to pass results between nodes
        self.results={}

        # a list of all nodes
        self.nodes=[]

        # current state of the network
        self.state={}

    def connect(self,from_node, from_name, to_node, to_name):
        forward_flow=getattr(from_node,from_name)
        setattr(to_node,to_name,forward_flow)

        backward_flow=getattr(to_node,"d_"+to_name)
        setattr(from_node,"d_"+from_name,backward_flow)

    def register_input(self, shape, to_node, to_name):
        self.input=np.empty(shape)
        setattr(to_node,to_name,self.input)
        self.d_input=getattr(to_node,"d_"+to_name)

    def register_output(self, shape, from_node, from_name):
        self.output=getattr(from_node,from_name)
        self.d_output=np.ones(shape)
        setattr(from_node,"d_"+from_name,self.d_output)

    @cython.nonecheck(False)
    @cython.boundscheck(False)
    def forward(self):
        for layer in self.layers:
            for node in layer:
                node.forward()

    def backward(self):
        for layer in self.layers[::-1]:
            for node in layer:
                node.backward()

    def grad_check(self, logrange):
        #np.random.seed(0)
        for eps in np.logspace(-5,logrange,10):
            sample=np.random.rand(*self.input.shape)
            self.input[:] =sample
            self.forward()
            output1=np.empty(self.output.shape)
            output1[:]=self.output[:]

            self.backward()
            d_input=np.empty(self.d_input.shape)
            d_input[:]=self.d_input[:]

            tolerance=1e-5

            for mask in np.ndindex(self.input.shape):
                delta=np.zeros(self.input.shape)
                delta[mask]=1*eps
                self.input[:]=sample+delta
                self.forward()
                output2=np.empty(self.output.shape)
                output2[:]=self.output

                der_numeric=(output2-output1)/eps

                if not abs(der_numeric-d_input[mask])<tolerance:
                    return False

        return True


class GraphTest(unittest.TestCase):
    def test_node(self):
        
        #np.random.seed(0)
        shape=(2,2)
        g=Graph()
        g.layers.append([])
        g.layers.append([])
        n1=Node("n1",shape)
        n2=AddNode("n2",shape,-1)
        g.layers[0].append(n1)
        g.layers[1].append(n2)

        g.connect(n1,"y",n2,"x")
        input=np.random.rand(*shape)
        n1.x=input
        g.forward()
        output=n2.y

    def test_gradient(self):
        #np.random.seed(0)
        shape = (2, 2)
        g = Graph()
        g.layers.append([])
        g.layers.append([])
        n1 = Node("n1", shape)
        n2 = AddNode("n2", shape, -1)
        g.layers[0].append(n1)
        g.layers[1].append(n2)

        g.connect(n1, "y", n2, "x")

        g.register_input(shape,n1,"x")
        g.register_output(shape,n2,"y")

        for eps in [0.01,0.001,0.0001]:
            input1 = np.abs(np.random.rand(*shape))+0.1

            n1.x = input1
            g.forward()
            output1=np.empty((n2.y.shape))
            output1[:] = n2.y[:]

            eps=0.1
            input2=input1+eps

            n1.x=input2
            g.forward()
            output2=n2.y

            delta=output2-output1
            der=delta/eps

            grad_start=np.ones(shape)
            n2.d_y[:]=grad_start
            g.backward()
            der2=np.empty(shape)
            der2[:]=n1.d_x

            check=np.allclose(der2,der)
            if not check:
                print("just for debugging.")
            self.assertTrue(check)

    def test_addnode(self):
        #np.random.seed(0)
        shape = (2, 1)
        g = Graph()
        g.layers.append([])
        g.layers.append([])
        g.layers.append([])
        n1 = Node("n1", shape)
        n2=AddNode("n2",shape,-1)
        db=DebugNode("db",shape)
        g.layers[0].append(n1)
        g.layers[1].append(n2)
        g.layers[2].append(db)

        g.connect(n1,"y",n2,"x")
        g.connect(n2,"y",db,"x")

        g.register_input(shape, n1, "x")
        g.register_output((1,), db, "y")
        self.assertTrue(g.grad_check(-5))

    def test_linnode(self):
        np.random.seed(0)
        shape=(2,1)
        g=Graph()
        g.layers.append([])
        g.layers.append([])

        W=np.abs(np.random.rand(2,2))
        b=np.random.rand(*shape)
        weights=np.random.rand(2,1)
        ln=LinearNode("lin",shape,W,b)
        db = DebugNode("db",shape,weights)
        g.nodes.append(ln)
        g.nodes.append(db)
        g.layers[0].append(ln)
        g.layers[1].append(db)
        g.connect(ln,"y",db,"x")
        g.register_input(shape,ln,"x")
        g.register_output(shape,db,"y")

        self.assertTrue(g.grad_check(-7))

    def test_relunode(self):
        shape=(2,1)
        g=Graph()
        g.layers.append([])
        g.layers.append([])

        relu=ReluNode("relu",shape)
        db=DebugNode("db",shape,np.ones(shape))

        g.layers[0].append(relu)
        g.layers[1].append(db)

        g.nodes.append(relu)
        g.nodes.append(db)

        g.connect(relu,"y",db,"x")
        g.register_input(shape,relu,"x")
        g.register_output(shape,db,"y")

        self.assertTrue(g.grad_check(-7))

    def test_im2col(self):

        batch_size=5
        n_colors=3
        width=10
        height=10

        inshape=(batch_size,n_colors,width,height)

        g = Graph()
        g.layers.append([])
        g.layers.append([])

        im2col = Im2ColNode("im2col",batch_size,n_colors,width,height)
        db = DebugNode("db", (9*n_colors,batch_size*width*height))

        g.layers[0].append(im2col)
        g.layers[1].append(db)

        g.nodes.append(im2col)
        g.nodes.append(db)

        g.connect(im2col, "y", db, "x")
        g.register_input(inshape, im2col, "x")
        g.register_output(1, db, "y")

        self.assertTrue(g.grad_check(-7))

    def test_col2im(self):
        batch_size = 5
        n_colors = 3
        width = 10
        height = 10

        inshape = ( n_colors,batch_size* width* height)

        g = Graph()
        g.layers.append([])
        g.layers.append([])

        col2im = Col2ImNode("col2im", batch_size, n_colors, width, height)
        db = DebugNode("db", (batch_size,n_colors,width,height))

        g.layers[0].append(col2im)
        g.layers[1].append(db)

        g.nodes.append(col2im)
        g.nodes.append(db)

        g.connect(col2im, "y", db, "x")
        g.register_input(inshape, col2im, "x")
        g.register_output(1, db, "y")

        g.input[:]=np.random.randn(*inshape)
        g.forward()

        self.assertTrue(g.grad_check(-7))

    def test_conv_node(self):
        batch_size = 5
        in_colors = 3
        out_colors=2
        width = 10
        height = 10

        W=np.random.randn(out_colors,9*in_colors)
        b=np.random.randn(out_colors,1)

        inshape = (batch_size,in_colors,width,height)

        g = Graph()
        g.layers.append([])
        g.layers.append([])

        conv_node = ConvNode("conv",W,b,batch_size,in_colors,out_colors,width,height)
        db = DebugNode("db", (batch_size, out_colors, width, height))

        g.layers[0].append(conv_node)
        g.layers[1].append(db)

        g.nodes.append( conv_node)
        g.nodes.append(db)

        g.connect( conv_node, "y", db, "x")
        g.register_input(inshape,  conv_node, "x")
        g.register_output(1, db, "y")

        g.input[:] = np.random.randn(*inshape)
        g.forward()

        self.assertTrue(g.grad_check(-7))

    def test_conv_node_stride(self):
        batch_size = 5
        in_colors = 3
        out_colors = 2
        in_width = 10
        in_height = 10

        for i in range(4):
            for j in range(3):
                for k in range(4):
                    filter_size=i+1
                    stride=j+1
                    padding=k

                    if (in_width - filter_size + 2 * padding) % stride!=0:
                        continue

                    out_width = (in_width - filter_size + 2 * padding) / stride + 1
                    out_height = (in_height - filter_size + 2 * padding) / stride + 1

                    W = np.random.randn(out_colors, filter_size**2 * in_colors)
                    b = np.random.randn(out_colors, 1)

                    inshape = (batch_size, in_colors, in_width, in_height)

                    g = Graph()
                    g.layers.append([])
                    g.layers.append([])

                    conv_node = ConvNodeStride("conv",filter_size,padding,stride, W, b, batch_size, in_colors, out_colors, in_width, in_height)
                    db = DebugNode("db", (batch_size, out_colors, out_width, out_height))

                    g.layers[0].append(conv_node)
                    g.layers[1].append(db)

                    g.nodes.append(conv_node)
                    g.nodes.append(db)

                    g.connect(conv_node, "y", db, "x")
                    g.register_input(inshape, conv_node, "x")
                    g.register_output(1, db, "y")

                    g.input[:] = np.random.randn(*inshape)

                    print("---")
                    print(filter_size)
                    print(padding)
                    print(stride)
                    g.forward()

                    self.assertTrue(g.grad_check(-7))


