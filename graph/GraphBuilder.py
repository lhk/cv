import numpy as np
from graph.Graph import Graph
import numpy as np
import unittest
from graph.nodes.Node import Node
from graph.nodes.AddNode import AddNode
from graph.nodes.LinearNode import LinearNode
from graph.nodes.DebugNode import DebugNode
from graph.nodes.ReluNode import ReluNode
from graph.nodes.Im2ColNode import Im2ColNode
from graph.nodes.Col2ImNode import Col2ImNode
from graph.nodes.ConvNode import ConvNode
from graph.nodes.ConvNodeStride import ConvNodeStride
from graph.nodes.CrossEntropyLossNode import CrossEntropyLossNode
from graph.nodes.ReshapeNode import ReshapeNode
from graph.nodes.TransposeNode import TransposeNode
from graph.nodes.DropoutNode import DropoutNode
from graph.nodes.SoftmaxLossNode import SoftmaxLossNode


def build_fc_graph(w_factor,b_factor,batch_size,in_nodes, out_nodes):
    """

    :param w_factor: factor to scale the distribution of W
    :param b_factor: factor to scale the distribution of b
    :param batch_size: batch_size
    :param in_nodes: in_nodes
    :param out_nodes: out_nodes
    :return: graph containing the node setup
    """
    g=Graph()

    # a dropout node
    # a list of fc nodes
    #then a relu
    #then an sml
    #makes 1+ len(out_shapes) + 1 + 1 layers
    for i in range(len(out_nodes)+3):
        g.layers.append([])

    layernum=0
    shape_in=in_nodes
    prev=None

    #dp=DropoutNode("dropout",(in_nodes,batch_size),0.9)
    dp=Node("passthrough",(in_nodes,batch_size))
    g.register_input((in_nodes,batch_size),dp,"x")

    g.layers[layernum].append(dp)
    g.nodes.append(dp)
    prev=dp

    #this is used later to create the trainer object
    fc_nodes=[]


    for i in range(len(out_nodes)):
        shape_out=out_nodes[i]
        W=np.sqrt(2/shape_out)*(w_factor*np.random.randn(shape_out,shape_in) + w_factor)
        b=b_factor*np.random.randn(shape_out,1)
        fc=LinearNode("fc"+str(i),batch_size,shape_in,shape_out,W,b)
        fc_nodes.append(fc)
        shape_in=shape_out
        g.layers[layernum].append(fc)
        layernum+=1
        g.nodes.append(fc)

        #without a dropout node, there is no prev at this time
        #if i>0:
        #    g.connect(prev, "y", fc, "x")
        g.connect(prev,"y",fc,"x")
        prev=fc


    g.fc=fc

    rel=ReluNode("rel",(out_nodes[-1],batch_size))
    g.layers[layernum].append(rel)
    layernum+=1
    g.nodes.append(rel)
    g.connect(fc,"y",rel,"x")

    sml=SoftmaxLossNode("sml",batch_size,out_nodes[-1])
    g.layers[layernum].append(sml)
    layernum+=1
    g.nodes.append(sml)
    g.connect(rel,"y",sml,"x")

    g.register_output((1,batch_size),sml,"y")
    g.register_labels(sml,"labels")

    trainer = {}

    for i, node in enumerate(fc_nodes):
        trainer["W_fc"+str(i)]=[node.W,node.d_W,np.zeros(node.d_W.shape)]
        trainer["b_fc"+str(i)]=[node.b,node.d_b,np.zeros(node.d_b.shape)]

    return g, trainer

def rebuild_fc_graph(W_list,b_list,batch_size,in_nodes, out_nodes):
    """
    from existing matrices
    :param W: list of W matrices
    :param b: list of b vectors
    :param batch_size: batch_size
    :param in_nodes: in_nodes
    :param out_nodes: out_nodes
    :return: graph containing the node setup
    """
    g=Graph()

    # a list of fc nodes
    #then a relu
    #then an sml
    #makes len(out_shapes) + 1 + 1 layers
    for i in range(len(out_nodes)+2):
        g.layers.append([])

    layernum=0
    shape_in=in_nodes
    prev=None

    for i in range(len(out_nodes)):
        shape_out=out_nodes[i]
        W=W_list[i]
        b=b_list[i]
        fc=LinearNode("fc"+str(i),batch_size,shape_in,shape_out,W,b)
        shape_in=shape_out
        g.layers[layernum].append(fc)
        layernum+=1
        g.nodes.append(fc)
        if i>0:
            g.connect(prev,"y",fc,"x")
        prev=fc
    g.register_input((in_nodes,batch_size),g.nodes[0],"x")

    g.fc=fc

    rel=ReluNode("rel",(out_nodes[-1],batch_size))
    g.layers[layernum].append(rel)
    layernum+=1
    g.nodes.append(rel)
    g.connect(fc,"y",rel,"x")

    sml=SoftmaxLossNode("sml",batch_size,out_nodes[-1])
    g.layers[layernum].append(sml)
    layernum+=1
    g.nodes.append(sml)
    g.connect(rel,"y",sml,"x")

    g.register_output((1,batch_size),sml,"y")
    g.register_labels(sml,"labels")

    trainer = {}

    for i in range(len(out_nodes)):
        trainer["W_fc"+str(i)]=[g.nodes[i].W,g.nodes[i].d_W,np.zeros(g.nodes[i].d_W.shape)]
        trainer["b_fc"+str(i)]=[g.nodes[i].b,g.nodes[i].d_b,np.zeros(g.nodes[i].d_b.shape)]

    return g, trainer


