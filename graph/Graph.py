import numpy as np
import unittest
from graph.nodes.Node import Node
from graph.nodes.AddNode import AddNode
from graph.nodes.LinearNode import LinearNode
from graph.nodes.DebugNode import DebugNode
from graph.nodes.ReluNode import ReluNode
from graph.nodes.Im2ColNode import Im2ColNode
from graph.nodes.Col2ImNode import Col2ImNode
from graph.nodes.ConvNode import ConvNode
from graph.nodes.ConvNodeStride import ConvNodeStride
from graph.nodes.CrossEntropyLossNode import CrossEntropyLossNode
from graph.nodes.ReshapeNode import ReshapeNode
from graph.nodes.TransposeNode import TransposeNode
from graph.nodes.RelativeFreqNode import RelativeFreqNode
from graph.nodes.SoftmaxLossNode import SoftmaxLossNode
from graph.nodes.DropoutNode import DropoutNode


import warnings
import time

"""
interesting ideas: create a node specifically for input purposes
"""
class Graph:
    def __init__(self):
        # a list of lists of nodes
        self.layers=[]

        # a dictionary to pass results between nodes
        self.results={}

        # a list of all nodes
        self.nodes=[]

        # current state of the network
        self.state={}

    def connect(self,from_node, from_name, to_node, to_name):
        forward_flow=getattr(from_node,from_name)
        setattr(to_node,to_name,forward_flow)

        backward_flow=getattr(to_node,"d_"+to_name)
        setattr(from_node,"d_"+from_name,backward_flow)

    def register_input(self, shape, to_node, to_name):
        self.input=np.empty(shape)
        setattr(to_node,to_name,self.input)
        self.d_input=getattr(to_node,"d_"+to_name)

    def register_output(self, shape, from_node, from_name):
        self.output=getattr(from_node,from_name)
        self.d_output=np.ones(shape)
        setattr(from_node,"d_"+from_name,self.d_output)

    def register_labels(self, to_node, to_name):
        self.labels=getattr(to_node,to_name)

    def forward(self):
        for layer in self.layers:
            for node in layer:
                node.forward()

    def backward(self):
        for layer in self.layers[::-1]:
            for node in layer:
                node.backward()

    def grad_check(self, logrange):
        #np.random.seed(0)
        for eps in np.logspace(-5,logrange,10):
            sample=np.random.rand(*self.input.shape)
            self.input[:] =sample
            self.forward()
            output1=np.empty(self.output.shape)
            output1[:]=self.output[:]

            self.backward()
            d_input=np.empty(self.d_input.shape)
            d_input[:]=self.d_input[:]

            tolerance=1e-4

            for mask in np.ndindex(self.input.shape):
                delta=np.zeros(self.input.shape)
                delta[mask]=1*eps
                self.input[:]=sample+delta
                self.forward()
                output2=np.empty(self.output.shape)
                output2[:]=self.output

                der_numeric=(output2-output1)/eps

                if not abs(der_numeric-d_input[mask])<tolerance:
                    return False

        return True

