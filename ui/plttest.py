import matplotlib.pyplot as plt
import numpy as np
import time

plt.ion()
plt.pause(0.01)
x = np.arange(0, 4*np.pi, 0.1)
y = np.sin(x)
ln,=plt.plot(x, y, 'g-', linewidth=1.5, markersize=4)
plt.pause(0.1)
plt.show()
while True:
    print(time.time())
    x+=0.1
    ln.set_ydata(np.sin(x))
    plt.pause(0.000001)