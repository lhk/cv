"""
set up graph for neural network
"""
from graph.Graph import Graph
import numpy as np
from graph.nodes.LinearNode import LinearNode
from graph.nodes.ReluNode import ReluNode
from graph.nodes.ConvNodeNumba import ConvNodeStride
from graph.nodes.ReshapeNode import ReshapeNode
from graph.nodes.TransposeNode import TransposeNode
from graph.nodes.SoftmaxLossNode import SoftmaxLossNode
import pickle
import graph.GraphBuilder

f1=open("../graph/training_results100lrate1e-05decay0.85penalty0.0001[70, 50, 35, 10].pickle","rb")
f2=open("../graph/training_results100lrate1e-05decay0.85penalty0.0001[70, 50, 35, 10].pickle","rb")

trainer1=pickle.load(f1)
trainer2=pickle.load(f2)

W=[0,1,2,3]
b=[0,1,2,3]

for key, val in trainer1.items():
    print(key)
    if "W" in key:
        other_val=trainer2[key]
        id=int(key[-1])
        print(id)
        W[id]=((val[0]))#+other_val[0])/2)
    else:
        other_val=trainer2[key]
        id = int(key[-1])
        b[id]=((val[0]))#+other_val[0])/2)



batch_size=1
in_nodes_1=784

layer_sizes=[x.shape[0] for x in W]

g,trainer=graph.GraphBuilder.rebuild_fc_graph(W,b,batch_size,784,layer_sizes)

from tkinter import *
import numpy as np

width = 28**2
height = 28**2
nX = 28
nY = 28
shape = (nX, nY)
dx = width / nX
dy = width / nY

assert (width % nX == 0)
assert (height % nY == 0)

field = np.zeros((nX, nY))
field_rectangles = []

def setup_grid():
    for i in range(nX):
        field_rectangles.append([])
        for j in range(nY):
            x = i * dx
            y = j * dy
            fill = "black"
            rect = w.create_rectangle(x, y, x + dx, y + dy, fill=fill)
            field_rectangles[i].append(rect)


def update_grid():
    for i in range(nX):
        for j in range(nY):
            val = field[i][j]
            if val==0:
                fill="black"
            else:
                fill="white"
            w.itemconfig(field_rectangles[i][j], fill=fill)

def update_selective_grid(ij):
    val = field[ij]
    i=ij[0]
    j=ij[1]
    if val == 0:
        fill = "black"
    else:
        fill = "white"
    w.itemconfig(field_rectangles[i][j],fill=fill)



def left_click(event):
    print("lclick at", event.x, ", ", event.y)
    i, j = get_indices(event.x, event.y)
    field[i, j] = 1
    update_selective_grid((i,j))


def right_click(event):
    print("rclick at", event.x, ", ", event.y)
    i, j = get_indices(event.x, event.y)
    field[i, j] = 0
    update_selective_grid((i,j))

def get_indices(x, y):
    i = int(x / dx)
    j = int(y / dy)
    return i, j



root = Tk()
w = Canvas(root, width=width, height=height)
w.pack()
w.bind("<Button-1>", left_click)
w.bind("<B1-Motion>", left_click)
w.bind("<Button-3>", right_click)
w.bind("<B3-Motion>",right_click)
root.bind("<Return>", lambda x: calculate())
root.bind("<Delete>", lambda x: reset())

# removing sml node
del g.nodes[-1]
del g.layers[-1]

def reset():
    field[:]=0
    update_grid()


def calculate():
    g.labels[:]=np.ones(g.labels.shape)
    inpt=field.T.reshape(1,28**2).T
    g.input[:]=inpt
    #g.forward()
    g.nodes[-1].d_y[:]=np.zeros((10,1))
    g.nodes[-1].d_y[1]=1
    g.backward()
    data=g.nodes[0].d_x.reshape((28,28))

    field[:]=0
    factor=1
    nums=50
    tol=1.2
    for i in range(10):
        highlighted_fields=np.sum(data>np.mean(data)*factor)

        if highlighted_fields>nums*tol:
            factor*=np.power(highlighted_fields/nums, 0.2)+1e-4
        elif highlighted_fields<nums/tol:
            factor*=np.power(highlighted_fields/nums, 0.2)-1e-5
        else:
            break

    field[data>np.mean(data)*factor]=1
    update_grid()
    print(data)
    print("test")

    #pred=np.argmax(g.fc.y[:,0])
    #print(pred)


setup_grid()
update_grid()
mainloop()
update_grid()
