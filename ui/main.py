"""
set up graph for neural network
"""
from graph.Graph import Graph
import numpy as np
from graph.nodes.LinearNode import LinearNode
from graph.nodes.ReluNode import ReluNode
from graph.nodes.ConvNodeNumba import ConvNodeStride
from graph.nodes.ReshapeNode import ReshapeNode
from graph.nodes.TransposeNode import TransposeNode
from graph.nodes.SoftmaxLossNode import SoftmaxLossNode

batch_size=1

# I intend to do the following:
# 1: input layer (batch_size, 784)
# 2: reshape (batchsize,1,28,28)
# 3: ConvNode: filtersize=3, stride=0, padding=1
#              out_features=4
# The idea is to allow checking for borders - | \ /
# 4: FC Node
# 5: CrossEntropy Loss

g=Graph()

#add layers to the graph
#reshape
#conv
#reshape
#transpose
#fc
#relu
#crossentropy
for i in range(7):
    g.layers.append([])

#1st node: Reshape
shape_01=(batch_size,784)

#now transpose it
shape_12=(784, batch_size)
trans=TransposeNode("transpose",shape_01,(1,0))
g.layers[0].append(trans)
g.register_input(shape_01,trans,"x")

#now we can apply the FC node
#from the out_cols*in_height*in_width applied filters
#we need to determine the probability for each class

in_nodes_1=784
out_nodes_1=50
#TODO: move init of W and b somewhere centralized
W1=np.load("../graph/W_fc1.npy")
b1=np.load("../graph/b_fc1.npy")
shape_23=(out_nodes_1, batch_size)
fc1=LinearNode("fc1", batch_size, in_nodes_1, out_nodes_1, W1, b1)
g.layers[1].append(fc1)
g.connect(trans,"y",fc1,"x")

#6st node: ReluNode
rel1=ReluNode("relu",shape_23)
g.layers[2].append(rel1)
g.connect(fc1,"y",rel1,"x")

in_nodes_2=50
out_nodes_2=10
#TODO: move init of W and b somewhere centralized
W2=np.load("../graph/W_fc2.npy")
b2=np.load("../graph/b_fc2.npy")
shape_45=(out_nodes_2, batch_size)
fc2=LinearNode("fc", batch_size, in_nodes_2, out_nodes_2, W2, b2)
g.layers[3].append(fc2)
g.connect(rel1,"y",fc2,"x")

#6st node: ReluNode
rel2=ReluNode("relu",shape_45)
g.layers[4].append(rel2)
g.connect(fc2,"y",rel2,"x")


"""
#set up convnnet
g=Graph()

#add layers to the graph
#reshape
#conv
#reshape
#transpose
#fc
#relu
#crossentropy
for i in range(7):
    g.layers.append([])

#1st node: Reshape
shape_01=(batch_size,784)
shape_12=(batch_size,1,28,28)
rn_input=ReshapeNode("reshape input", shape_01, shape_12)
g.layers[0].append(rn_input)
g.register_input(shape_01, rn_input, "x")

#2nd node: Convolution
filter_size=3
padding=1
stride=0
in_cols=1
out_cols=4
in_width=28
in_height=28

W=np.load("../graph/W_conv.npy")
b=np.load("../graph/b_conv.npy")

cnv=ConvNodeStride("convnode",
                   filter_size,padding,stride,
                   W,b,
                   batch_size,in_cols,out_cols,
                   in_width,in_height)
g.layers[1].append(cnv)
g.connect(rn_input, "y", cnv, "x")

#3rd node: FC node (actually three nodes)
#There is a conflict in the layout of the nodes
#The LinearNode(fully connected) requires this shape:
#(nodes, batch_size)
#I'll add a reshape and a transpose node, too


#reshape the output from the ConvNode
shape_23=(batch_size,out_cols,in_width,in_height)
shape_34=(batch_size,out_cols*in_width*in_height)
rs_conv=ReshapeNode("rs conv",shape_23,shape_34)
g.layers[2].append(rs_conv)
g.connect(cnv,"y",rs_conv,"x")

#now transpose it
shape_45=(out_cols*in_height*in_width,batch_size)
trans=TransposeNode("transpose",shape_34,(1,0))
g.layers[3].append(trans)
g.connect(rs_conv,"y",trans,"x")

#now we can apply the FC node
#from the out_cols*in_height*in_width applied filters
#we need to determine the probability for each class
n_classes=10
in_nodes=out_cols*in_height*in_width

W=np.load("../graph/W_fc.npy")
b=np.load("../graph/b_fc.npy")

shape_56=(n_classes,batch_size)
fc=LinearNode("fc",batch_size,in_nodes,n_classes,W,b)
g.layers[4].append(fc)
g.connect(trans,"y",fc,"x")

#6st node: ReluNode
rel=ReluNode("relu",shape_56)
g.layers[5].append(rel)
g.connect(fc,"y",rel,"x")
"""
#7th node: CrossEntropyLoss
# TODO: determine if this can just be left out.
"""
sml=SoftmaxLossNode("softmaxloss",batch_size,n_classes)
g.layers[6].append(sml)
g.connect(rel,"y",sml,"x")
g.register_labels(sml,"labels")
g.register_output((1,batch_size),sml,"y")
"""
from tkinter import *
import numpy as np

width = 28**2
height = 28**2
nX = 28
nY = 28
shape = (nX, nY)
dx = width / nX
dy = width / nY

assert (width % nX == 0)
assert (height % nY == 0)

field = np.zeros((nX, nY))
field_rectangles = []

def setup_grid():
    for i in range(nX):
        field_rectangles.append([])
        for j in range(nY):
            x = i * dx
            y = j * dy
            fill = "black"
            rect = w.create_rectangle(x, y, x + dx, y + dy, fill=fill)
            field_rectangles[i].append(rect)


def update_grid():
    for i in range(nX):
        for j in range(nY):
            val = field[i][j]
            if val==0:
                fill="black"
            else:
                fill="white"
            w.itemconfig(field_rectangles[i][j], fill=fill)

def update_selective_grid(ij):
    val = field[ij]
    i=ij[0]
    j=ij[1]
    if val == 0:
        fill = "black"
    else:
        fill = "white"
    w.itemconfig(field_rectangles[i][j],fill=fill)



def left_click(event):
    print("lclick at", event.x, ", ", event.y)
    i, j = get_indices(event.x, event.y)
    field[i, j] = 1
    update_selective_grid((i,j))


def right_click(event):
    print("rclick at", event.x, ", ", event.y)
    i, j = get_indices(event.x, event.y)
    field[i, j] = 0
    update_selective_grid((i,j))

def get_indices(x, y):
    i = int(x / dx)
    j = int(y / dy)
    return i, j



root = Tk()
w = Canvas(root, width=width, height=height)
w.pack()
w.bind("<Button-1>", left_click)
w.bind("<B1-Motion>", left_click)
w.bind("<Button-3>", right_click)
w.bind("<B3-Motion>",right_click)
root.bind("<Return>", lambda x: calculate())
root.bind("<Delete>", lambda x: reset())

def reset():
    field[:]=0
    update_grid()


def calculate():
    inpt=field.T.reshape(1,28**2)
    g.input[:]=inpt
    g.forward()
    pred=np.argmax(fc2.y[:,0])
    print(pred)
setup_grid()
update_grid()
mainloop()
update_grid()
